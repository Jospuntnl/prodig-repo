# Date:         14-10-2020
# Description: Script File For Automatic Simulation
# Author:      Jos Knol
# Based on: 	Simulation scripts by Jesse op den Brouw
# Version:      1.0

# Set transcript on
transcript on

# Recreate the work directory and map to work
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

# Compile the Double Dabble VHDL description and testbench
vcom -93 -work work ../../adc.vhd
vcom -93 -work work ../../average.vhd
vcom -93 -work work ../../clock.vhd
vcom -93 -work work ../../debouncer.vhd
vcom -93 -work work ../../deler.vhd
vcom -93 -work work ../../Displaydriver.vhd
vcom -93 -work work ../../Hometrainer.vhd
vcom -93 -work work ../../lcd_driver_hd44780_module.vhd
vcom -93 -work work ../../lcdcontroller.vhd
vcom -93 -work work ../../prescaler.vhd
vcom -93 -work work ../../rpmselector.vhd
vcom -93 -work work ../../rpmsens.vhd
vcom -93 -work work ../../state_machine.vhd
vcom -93 -work work ../../teller.vhd
vcom -93 -work work ../../up_down_servo.vhd
vcom -93 -work work ../../tb_hometrainer.vhd

# Start the simulator with 1 ns time resolution
vsim -t 1ns -L rtl_work -L work -voptargs="+acc" tb_hometrainer

# Log all signals in the design, good if the number
# of signals is small.
add log -r *



# Add all toplevel signals
# Add a number of signals of the simulated design
add wave -divider "SYSTEM"
add wave CLOCK_50
add wave dut/sysclk
add wave dut/reset




add wave -divider "Inputs"
add wave areset
add wave knop5

add wave -divider "Outputs"
add wave dut/minuten
add wave dut/seconden

# Open Structure, Signals (waveform) and List window
view structure
view list
view signals
view wave

# Run simulation for ...
run 2.0 sec

# Fill up the waveform in the window
wave zoom full
