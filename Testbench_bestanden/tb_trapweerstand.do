# Author: Ruben La Lau
# Student electrical engineering at The Hague University
# Date: 12-10-2020


# Set transcript on
transcript on

# Recreate the work directort and map to work
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

# Find out if we're started through Quartus or by hand
#(or by using an exec in the Tcl window in Quartus).
# Quartus has the annoying property that it will start
# Modelsim from a directory called "simulati
#on/modelsim".
# The design and the testbench are located in the project
# root, so we've to compensate for that.
if [ string match "*simulation/modelsim" [pwd] ] { 
	set prefix "../../"
	puts "Running Modelsim from Quartus..."
} else {
	set prefix ""
	puts "Running Modelsim..."
}

# Compile the VHDL description and testbench
vcom -93 -work work ${prefix}trapweerstand.vhd
vcom -93 -work work ${prefix}trapweerstandselector.vhd
vcom -93 -work work ${prefix}adc.vhd
vcom -93 -work work ${prefix}tb_trapweerstand.vhd

# Start simulator
vsim -t 1ns -L rtl_work -L work -voptargs="+acc"  tb_up_down_servo

# Log all signals in the design, good if the number of signals is small
add log -r *

# Add all toplevel signals
# Add a number of signals of the simulated design
add list clk
add list areset
add list paddleres
add list input_up
add list input_down
add list out_up
add list out_down

# Add all toplevel signals
# Add a number of signals of the simulated design
add wave -label input_up
add wave -label input_down
add wave -label clk	clk
add wave -label areset	areset
add wave -label paddleres	paddleres
add wave -label motor_up	motor_up
add wave -label motor_down	motor_down
add wave -label sysclear sysclear
add wave -label busy busy

# Open list and waveform window
view list
view wave

# Run simulation for 5000 ns
run 5000 ns

# Fill up the waveform in the window
wave zoom full