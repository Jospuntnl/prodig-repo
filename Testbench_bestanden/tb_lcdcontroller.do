-- Filename:     tb_lcdcontroller.do 
-- Filetype:     VHDL testbench
-- Date:         24/09/2020
-- Description:  A testbench of lcdcontroller
-- Author:       T.westmaas

# Set transcript on
transcript on

# Set up RTL work library
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}

# Create RTL work library and use it as work
vlib rtl_work
vmap work rtl_work

# Compile VHDL description of rpmselector
vcom -93 -work work lcd_driver_hd44780_module.vhd
vcom -93 -work work lcdcontroller.vhd

# Compile VHDL testbench of shifter
vcom -93 -work work tb_lcdcontroller.vhd

# Start the simulator with 1 ns time resolution
#   -t 1ns = 1 ns time step
#   -L ...= use library ...
vsim -t 1ns -L rtl_work -L work -voptargs="+acc"  tb_lcdcontroller

# Log all signals in the design, good if the number of signals is small.
add log -r *

# Add all toplevel and simulated device signals to the list view
add list clk
add list areset

add list rpmmode
add list rpmcount

add list paddleres

add list min
add list sec
add list maxrpmM
add list maxrpmS

add list lcd_gpio_E
add list lcd_gpio_RS
add list lcd_gpio_RW
add list lcd_gpio_D


# Add all toplevel signals and a number of signals inside
# the simulated design to the wave view
add wave -divider "Inputs"
add wave clk
add wave areset
add wave rpmmode
add wave rpmcount
add wave paddleres
add wave min
add wave sec
add wave maxrpmM
add wave maxrpmS

add wave -divider "Outputs"
add wave lcd_gpio_E
add wave lcd_gpio_RS
add wave lcd_gpio_RW
add wave lcd_gpio_D

# Open the List and Waveform window
view list
view wave

# Run simulation for 30 s
run 30000 ms

# Fill up the waveform in the window
wave zoom full