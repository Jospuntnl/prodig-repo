
# Set transcript on
transcript on

# Set up RTL work library
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}

# Create RTL work library and use it as work
vlib rtl_work
vmap work rtl_work

# Compile VHDL description of debouncer
vcom -93 -work work debouncer.vhd 


# Compile VHDL testbench of debouncer
vcom -93 -work work tb_debouncer.vhd 

# Start the simulator with 1 ns time resolution
#   -t 1ns = 1 ns time step
#   -L ...= use library ...
vsim -t 1ns -L rtl_work -L work -voptargs="+acc" tb_debouncer

# Log all signals in the design, good if the number of signals is small.
add log -r *

# Add all toplevel and simulated device signals to the list view
add list clk
add list areset
add list knop1
add list knop2
add list knop3
add list knop4
add list knop5
add list knop6

add list reset
add list modebutton
add list input_up
add list input_down
#add list start_stop
#add list Recovery

# Add all toplevel signals and a number of signals inside
# the simulated design to the wave view
add wave -divider "Inputs"
add wave clk
add wave areset
add wave knop1
add wave knop2
add wave knop3
add wave knop4
add wave knop5
add wave knop6

add wave -divider "Outputs"
add wave reset
add wave modebutton
add wave input_up
add wave input_down
#add wave start_stop
#add wave Recovery


# Open the List and Waveform window
view list
view wave

# Run simulation for 10000 ms
run 10000 ms

# Fill up the waveform in the window
wave zoom full