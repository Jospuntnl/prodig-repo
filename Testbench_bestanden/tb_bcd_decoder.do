-- Filename:     tb_bcd_decoder.do 
-- Filetype:     VHDL testbench
-- Date:         24/09/2020
-- Description:  A testbench of bcd_decoder
-- Author:       T.westmaas

# Set transcript on
transcript on

# Set up RTL work library
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}

# Create RTL work library and use it as work
vlib rtl_work
vmap work rtl_work

# Compile VHDL description of bcd_decoder
vcom -93 -work work tb_bcd_decoder.vhd

# Compile VHDL testbench of shifter
vcom -93 -work work tb_bcd_decoder.vhd

# Start the simulator with 1 ns time resolution
#   -t 1ns = 1 ns time step
#   -L ...= use library ...
vsim -t 1ns -L rtl_work -L work -voptargs="+acc"  tb_bcd_decoder

# Log all signals in the design, good if the number of signals is small.
add log -r *

# Add all toplevel and simulated device signals to the list view
add list clk

add list binary_in
add list bcd_out



# Add all toplevel signals and a number of signals inside
# the simulated design to the wave view
add wave -divider "Inputs"
add wave clk
add wave binary_in

add wave -divider "Outputs"
add wave bcd_out

# Open the List and Waveform window
view list
view wave

# Run simulation for 30 s
run 10000 ms

# Fill up the waveform in the window
wave zoom full