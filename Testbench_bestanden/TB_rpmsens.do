# Date:         14-10-2020
# Description: Script File For Automatic Simulation
# Author:      Jos Knol
# Based on: 	Simulation scripts by Jesse op den Brouw
# Version:      1.0

# Set transcript on
transcript on

# Recreate the work directory and map to work
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

# Compile the Double Dabble VHDL description and testbench
vcom -93 -work work ../../deler.vhd
vcom -93 -work work ../../teller.vhd
vcom -93 -work work ../../rpmsens.vhd
vcom -93 -work work ../../TB_rpmsens.vhd

# Start the simulator with 1 ns time resolution
vsim -t 1ns -L rtl_work -L work -voptargs="+acc" TB_rpmsens

# Log all signals in the design, good if the number
# of signals is small.
add log -r *



# Add all toplevel signals
# Add a number of signals of the simulated design
add wave -divider "SYSTEM"
add wave areset
add wave clk
add wave sysclear
add wave dut/pulse

add wave -divider "Inputs"
add wave hallsensor


add wave -divider "Outputs"

add wave rpm
add wave rotations
add wave max_rpm
add wave maxflag


# Open Structure, Signals (waveform) and List window
view structure
view list
view signals
view wave

# Run simulation for ...
run 1.5 sec

# Fill up the waveform in the window
wave zoom full
