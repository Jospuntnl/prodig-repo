# Filename:     tb_clock.do
# Filetype:     Modelsim Script File
# Date:         9 mar 2014
# Update:       -
# Description:  Script File For Automatic Simulation
# Author:       J. op den Brouw
# State:        Demo
# Error:        -
# Version:      1.2
# Copyright:    (c)2014, De Haagse Hogeschool

# Transcript window
transcript on

# Recreate working directories. Quartus wants an RTL
# work directory for RTL simulation.
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
# Create RTL work library and use it as work
vlib rtl_work
vmap work rtl_work

# Find out if we're started through Quartus or by hand
#(or by using an exec in the Tcl window in Quartus).
# Quartus has the annoying property that it will start
# Modelsim from a directory called "simulation/modelsim".
# The design and the testbench are located in the project
# root, so we've to compensate for that.
if [ string match "*simulation/modelsim" [pwd] ] { 
	set prefix "../../"
	puts "Running Modelsim from Quartus..."
} else {
	set prefix ""
	puts "Running Modelsim..."
}

# Compile full adder and its testbench, please note that
# the design and its testbench are located in the project
# root, but the simulator starts in directory
# <project_root>/simulation/modelsim, so we have to compensate
# for that.
vcom -93 -work work ${prefix} clock.vhd
vcom -93 -work work ${prefix} tb_clock.vhd

# Start simulation
vsim -t 1ns -L rtl_work -L work -voptargs="+acc"  tb_clock

# Log all signals in the design, good if the number
# of signals is small.
add log -r *

# Add all toplevel signals
# Add a number of signals of the simulated design
add list clk
add list sysclear
add list reset
add list start_stop
add list maxflag
add list seconds
add list minutes
add list secpuls
add list maxrpmM
add list maxrpmS

# Add all toplevel signals
# Add a number of signals of the simulated design
add wave -divider "Input"
add wave -label start_stop start_stop
add wave -label clk clk
add wave -label reset reset
add wave -label sysclear sysclear
add wave -label maxflag maxflag
add wave -divider "output"
if {[find signals dut/current_state] != ""} {
	add wave dut/current_state
}
add wave -radix unsigned seconds
add wave -label minutes minutes
add wave -label secpuls secpuls
add wave -label maxrpmM maxrpmM
add wave -label maxrpmS maxrpmS

add wave -divider "System"



# Open List and Waveform window
view list
view wave

# Run simulation for 70 sec
run 70 sec

# Scale wave window to fulle zoom
wave zoom full
