# Set transcript on
transcript on

# Recreate the work directory and map to work
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

# Compile the Double Dabble VHDL description and testbench
vcom -93 -work work ../../adc.vhd
vcom -93 -work work ../../tb_adc.vhd

# Start the simulator with 1 ns time resolution
vsim -t 1ns -L rtl_work -L work -voptargs="+acc" tb_adc

# Log all signals in the design, good if the number
# of signals is small.
add log -r *

# Add all toplevel signals
# Add a number of signals of the simulated design
add list clk 	
add list areset
add list busy
add list data
add list readdata
add list dataout
add list convstart
add list ready
add list out_up
add list out_down

# Add all toplevel signals
# Add a number of signals of the simulated design
add wave -divider "Input"
add wave -label clk	clk
add wave -label areset	areset
add wave -divider "overige"
add wave -label busy busy
add wave -label data data
add wave -divider "output"
if {[find signals dut/current_state] != ""} {
	add wave dut/current_state
}
add wave -label readdata readdata
add wave -label dataout dataout
add wave -label convstart convstart
add wave -label ready ready
add wave -label out_up out_up
add wave -label out_down out_down
if {[find signals dut/current_state] != ""} {
	add wave dut/current_state
}

# Open list and waveform window
view list
view wave

# Run simulation for 6000 ns
run 6000 us

# Fill up the waveform in the window
wave zoom full