# Date:         14-10-2020
# Description: Script File For Automatic Simulation
# Author:      Jos Knol
# Based on: 	Simulation scripts by Jesse op den Brouw
# Version:      1.0


# Set transcript on
transcript on

# Recreate the work directory and map to work
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

# Compile the Double Dabble VHDL description and testbench
vcom -93 -work work ../../teller.vhd
vcom -93 -work work ../../TB_teller.vhd

# Start the simulator with 1 ns time resolution
vsim -t 1ns -L rtl_work -L work -voptargs="+acc" TB_teller

# Log all signals in the design, good if the number
# of signals is small.
add log -r *



# Add all toplevel signals
# Add a number of signals of the simulated design
add wave -divider "SYSTEM"
add wave clk
add wave areset
add wave sysclear

add wave -divider "Inputs"
add wave hallsens

add wave -divider "internal"
add wave dut/s1
add wave dut/s2
add wave dut/s3
add wave dut/dout
add wave dut/counter
add wave dut/tempcount
add wave dut/lastcount


add wave -divider "Outputs"
add wave counter
add wave rotations

# Open Structure, Signals (waveform) and List window
view structure
view list
view signals
view wave

# Run simulation for ...
run 1.2 sec

# Fill up the waveform in the window
wave zoom full
