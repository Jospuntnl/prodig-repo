# README #



### Inleiding ###
Dit repository bevat VHDL beschrijvingen voor een hometrainer. Dit project is onderdeel van het vak [PRODIG](https://ds.opdenbrouw.nl/prodig.html) (Project digitale techniek) op de opleiding elektrotechniek van de Haagse Hogeschool.




### Hoe zit deze repository in elkaar? ###

* De map VHDL_bestanden bevat alle VHDL beschrijvingen.

* De map testbench_bestanden bevat alle testbench bestanden om de VHDL beschrijvingen te testen.

* Verder bestaat dit repository uit door de [software](https://www.intel.com/content/www/us/en/software/programmable/quartus-prime/overview.html) gegenereerde bestanden voor het compileren, testen, e.d. van de beschrijvingen.


### Contactgegevens ###

Voor vragen en/of opmerkingen: [josknol@outlook.com](mailto:josknol@outlook.com).