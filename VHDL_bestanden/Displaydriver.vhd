-- Filename:  Displaydriver.vhd   
-- Filetype:  vhdl
-- Date:         24/09/2020
-- Description:  VHDL Description 
-- Author:       T.westmaas


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- clock_10  - input clock (10 kHz)
-- reset     - asynchronous reset
-- mode		 - mode selection
--	paddleres - 0 tot 7 trapweerstand
-- huidigrpm - het datetwerkelijke RPM
-- maxrpm	 - Maximum RPM 
-- gemrpm	 - gemiddelde RPM
-- omwcount  - aantal gemaakte omwentelingen 
-- min		 - minuten
-- sec		 - seconden
-- lcd_gpio  - uitgangen voor de LCD

entity Displaydriver is

	port (CLOCK_10 : in std_logic;
			areset	: in std_logic;
			--trapweerstand
			paddleres  : in std_logic_vector(3 downto 0);
			calready	  : in std_logic;
			--RPM 
			modebutton	: in std_logic;
			huidigrpm	: in std_logic_vector(7 downto 0);
			maxrpm   	: in std_logic_vector(7 downto 0);
			gemrpm   	: in std_logic_vector(7 downto 0);
			omwcount 	: in std_logic_vector(15 downto 0);
			adcdata		: in std_logic_vector(7 downto 0);
			--tijd
			maxrpmM	: in std_logic_vector(7 downto 0);
			maxrpmS	: in std_logic_vector(7 downto 0);
			min   	: in std_logic_vector(7 downto 0);
			sec   	: in std_logic_vector(7 downto 0);
			-- LCD outputs
			lcd_gpio_E   : out std_logic;
			lcd_gpio_RS  : out std_logic;
			lcd_gpio_RW  : out std_logic;
			lcd_gpio_D 	 : inout std_logic_vector(7 downto 0)
			
			
		  );
end entity Displaydriver;



architecture hardware of Displaydriver is

component rpmselector is
    port (clk     : in std_logic;
			areset	: in std_logic;
			mode	   : in std_logic;
			huidigrpm: in std_logic_vector(7 downto 0);
			maxrpm   : in std_logic_vector(7 downto 0);
			gemrpm   : in std_logic_vector(7 downto 0);
			omwcount : in std_logic_vector(15 downto 0);
			adcdata	: in std_logic_vector(7 downto 0);
			rpmmode	: out std_logic_vector(2 downto 0);
			rpmcount : out std_logic_vector(15 downto 0)
         );
end component rpmselector;

component bcd_decoder is
    Port (  binary_in 	: in  std_logic_vector (15 downto 0);
				bcd_out 		: out  std_logic_vector (19 downto 0)
          );
end component bcd_decoder;


component lcdcontroller is
	port (CLOCK		: in std_logic;
			areset	: in std_logic;
			--rpm
			rpmmode	: in std_logic_vector(2 downto 0);
			rpmcount	: in std_logic_vector(19 downto 0);
			--paddle resistance 
			paddleres: in std_logic_vector(3 downto 0);
			calready	: in std_logic;
			--tijd
			maxrpmM	: in std_logic_vector(7 downto 0);
			maxrpmS	: in std_logic_vector(7 downto 0);
			min   	: in std_logic_vector(7 downto 0);
			sec   	: in std_logic_vector(7 downto 0);
			-- LCD 
			lcd_gpio_E   : out std_logic;
			lcd_gpio_RS  : out std_logic;
			lcd_gpio_RW  : out std_logic;
			lcd_gpio_D 	 : inout std_logic_vector(7 downto 0)
			
	);
	
end component lcdcontroller;

signal rpmmode_2_rpmmode		: std_logic_vector(2 downto 0);
signal rpmcount_2_binary_in: std_logic_vector(15 downto 0);
signal bcd_out_2_rpmcount,	bcd_out_2_maxrpmM, bcd_out_2_maxrpmS, bcd_out_2_min, bcd_out_2_sec				: std_logic_vector(19 downto 0);
signal forcenull				: std_logic_vector(7 downto 0);

begin
	forcenull <= "00000000";
	selector: rpmselector
	port map (clk => CLOCK_10, areset => areset, mode => modebutton, adcdata => adcdata, huidigrpm => huidigrpm, maxrpm => maxrpm, gemrpm => gemrpm, omwcount => omwcount, rpmmode => rpmmode_2_rpmmode, rpmcount => rpmcount_2_binary_in);

	
	--bcddecoders
	rpmbcddecoder: bcd_decoder
	port map ( binary_in => rpmcount_2_binary_in, bcd_out => bcd_out_2_rpmcount);
	
	maxrpmMbcddecoder: bcd_decoder
	port map ( binary_in(15 downto 8) => forcenull, binary_in(7 downto 0) => maxrpmM, bcd_out => bcd_out_2_maxrpmM);
	
	maxrpmSbcddecoder: bcd_decoder
	port map ( binary_in(15 downto 8) => forcenull, binary_in(7 downto 0) => maxrpmS, bcd_out => bcd_out_2_maxrpmS);
	
	minbcddecoder: bcd_decoder
	port map ( binary_in(15 downto 8) => forcenull, binary_in(7 downto 0) => min, bcd_out => bcd_out_2_min);
	
	secbcddecoder: bcd_decoder
	port map ( binary_in(15 downto 8) => forcenull, binary_in(7 downto 0) => sec, bcd_out => bcd_out_2_sec);
	
	controller: lcdcontroller
	port map (CLOCK => CLOCK_10, areset => areset,  rpmmode => rpmmode_2_rpmmode, rpmcount => bcd_out_2_rpmcount, paddleres => paddleres, calready => calready, min => std_logic_vector(bcd_out_2_min(7 downto 0)), sec => std_logic_vector(bcd_out_2_sec(7 downto 0)), maxrpmM => std_logic_vector(bcd_out_2_maxrpmM(7 downto 0)), maxrpmS => std_logic_vector(bcd_out_2_maxrpmS(7 downto 0)), lcd_gpio_E => lcd_gpio_E, lcd_gpio_RS => lcd_gpio_RS, lcd_gpio_RW => lcd_gpio_RW, lcd_gpio_D => lcd_gpio_D);
		-- STD_LOGIC_VECTOR(bcd(7 downto 4))
end architecture hardware;