--Datum: 9-10-20
--Auteur: Jos Knol
--Beschrijving:
--		Een testbench voor de deler
-- Versie: 1.1
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity TB_deler is
generic(SIZE: INTEGER := 32);
end entity;

architecture sim of TB_deler is

component deler is
	Port (
		countval		: in unsigned(31 downto 0);
		sysclear		: in std_logic;
		rpmo			: out unsigned(7 downto 0);
		areset		: in std_logic;
		clk			: in std_logic;
		maxrpm		: out unsigned(7 downto 0);
		maxflag		: out std_logic
		  ); 
end component deler;

signal sysclear : std_logic;
signal countval : unsigned(31 downto 0);
signal rpm		 : unsigned(7 downto 0);
signal areset	 : std_logic;
signal clk		 : std_logic;
signal maxrpm	 : unsigned(7 downto 0);
signal maxflag	 : std_logic;


begin

	dut: deler
	port map (sysclear => sysclear, clk => clk, rpmo => rpm, areset => areset, countval => countval, maxrpm => maxrpm, maxflag => maxflag);
	
	clockgen: process is	
	begin
	
		clk <= '0';
		
		wait for 10 us;
		
		loop
			clk <= '1';
			
			wait for 50 us;
			
			clk <= '0';
			
			wait for 50 us;
		end loop;
	end process;
	datagen: process is
		begin
		areset <= '1';
		sysclear <= '1';
		wait for 5 us;
		areset <= '0';
		sysclear <= '0';
		wait for 5 us;
		countval <= "00000000000000000000110101001000";
		wait for 10000 us;
		countval <= "00000000000000001101001101010000";
		wait for 10000 us;
		countval <= "00001000000000001101001101010000";
		wait for 10000 us;
		countval <= "00000000000000000000000000000100";
		wait for 10000 us;
		countval <= "00000000000000000000101111001111";

			 
			
			wait;
		end process;
end architecture;