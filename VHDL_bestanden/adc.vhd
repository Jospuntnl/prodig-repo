-- Author: Jos Knol
-- Student electrical engineering at The Hague University
-- Date: 12-10-2020

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--Entity van de adc.
entity adc is
	port( clk				: in std_logic;
			areset			: in std_logic;
			busy				: in std_logic;
			data				: in unsigned(7 downto 0);
			readdata			: out std_logic;
			dataout			: out unsigned(7 downto 0);
			convstart		: out std_logic;
			ready				: out std_logic;
			out_up			: out std_logic;
			out_down			: out std_logic
	);
end entity adc;

--architecture van state machine voor de adc.
architecture behav of adc is
type state_type is (s0, s1, s2, s3, s4, s5, s6);

type startupstatestate_type is (startup1, startup2, startup3, startup4, startup5, startup6, startup7, startup8, startup9, startup10, startupready);

signal state: state_type; 
signal startupstate: startupstatestate_type;

signal stand0, stand1, stand2, stand3, stand4, stand5, stand6, stand7 	: integer;


signal temp_out : unsigned(7 downto 0);
signal adcdata	 : unsigned(7 downto 0);


begin	
adc: process(clk, areset)
	begin
	if areset = '1' then
		state <= s0;
		temp_out <= "11111111";
	
	elsif rising_edge(clk) then
	
	case state is
			when s0 =>
				convstart <= '1';
				readdata <= '1';
				temp_out <= temp_out;
				adcdata  <= temp_out;
				state <= s1;
			
			when s1 =>
				convstart <= '0';
				readdata <= '1';
				temp_out <= temp_out;
				state <= s2;
		  
			when s2 =>
				convstart <= '0';
				readdata <= '1';
				temp_out <= temp_out;
				if busy = '0' then
					state <= s3;
				else
					state <= s2;
				end if;
				
			when s3 =>
				convstart <= '0';
				readdata <= '0';
				temp_out <= temp_out;
				state <= s4;
			
			when s4 =>
				convstart <= '0';
				readdata <= '0';
				temp_out (7 downto 0) <= data (7 downto 0);
				state <= s5;
			
			when s5 =>
				convstart <= '1';
				readdata <= '1';
				temp_out <= temp_out;
				state <= s6;
			
			when s6 =>
				convstart <= '1';
				readdata <= '1';
				temp_out <= temp_out;
				state <= s0;
			
			when others =>
				state <= s0;
				
		end case;
	end if;
end process;
	
startup: process(clk, areset)
variable adcvalue		: integer;
variable adccomp		: integer;
variable delay_counter : unsigned(15 downto 0);
variable count	 		:	unsigned(15 downto 0);

begin
	if areset = '1' then
		ready <= '0';
		startupstate <= startup1;
		count := "0000000000000000";
		delay_counter := "0000000000000000";
		
elsif rising_edge(clk) then
	adcvalue := to_integer(adcdata);
	
	if delay_counter = "0010011100010000" then
			delay_counter := "0000000000000000";
			adccomp := adcvalue;
	else
		delay_counter := delay_counter + 1;
	end if;
	
	
	
	case startupstate is
			when startup1 => 
			
				out_up <= '1';
				out_down <= '0';
				
				if count = "0100111000100000" then --voor 2 seconden
					out_up <= '0';
					count := "0000000000000000";
					stand7 <= adcvalue;
					startupstate <= startup2;
					
				elsif adccomp = adcvalue then
					count := count + 1;
				end if;
				
			when startup2 => 	
				
				out_down <= '1';
				
				if count = "0100111000100000" then --voor 2 seconden
					out_down <= '0';
					count := "0000000000000000";
					startupstate <= startup3;
					stand0 <= adcvalue;
					
				elsif adccomp = adcvalue then
					count := count + 1;
				end if;
				
			when startup3 => 
			
				adccomp := stand7 - stand0;
				startupstate <= startup4;
			
			when startup4 => 
			
				adccomp := adccomp / 7;
				startupstate <= startup5;
			
			when startup5 => 
			
				stand1 <= stand0 + adccomp;
				startupstate <= startup6;
			
			when startup6 => 
			
				stand2 <= stand1 + adccomp;
				startupstate <= startup7;
			
			when startup7 => 
			
				stand3 <= stand2 + adccomp;
				startupstate <= startup8;
			
			when startup8 => 
			
				stand4 <= stand3 + adccomp;
				startupstate <= startup9;
			
			when startup9 => 
			
				stand5 <= stand4 + adccomp;
				startupstate <= startup10;
			
			when startup10 => 
			
				stand6 <= stand5 + adccomp;
				startupstate <= startupready;
			
			when startupready =>
				ready <= '1';
				
			when others =>
				startupstate <= startup1;
		end case;
	end if;
end process;

conv: process(clk, areset) is
variable adcint		: integer;
begin
	if areset = '1' then
		dataout <= "00000000";
	elsif rising_edge(clk) then
		adcint := to_integer(adcdata);
		
			if stand0 = adcint then
				dataout <= 	"00000000";
			elsif stand1 = adcint then
				dataout <= 	"00000001";
			elsif stand2 = adcint then
				dataout <= 	"00000010";
			elsif stand3 = adcint then
				dataout <= 	"00000011";
			elsif stand4 = adcint then
				dataout <= 	"00000100";
			elsif stand5 = adcint then
				dataout <= 	"00000101";
			elsif stand6 = adcint then
				dataout <= 	"00000110";
			elsif stand7 = adcint then
				dataout <= 	"00000111";
			end if;
			
	end if;
end process;

end architecture;