library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clock is
generic(ClockFrequency : unsigned(15 downto 0) := "0010011100001111");	--clockFrequency is 10000
	port
	(
		clk 			: in	std_logic;
		sysclear		: in	std_logic;
		reset			: in	std_logic;
		start_stop	: in	std_logic;
		maxflag		: in 	std_logic;
		seconds		: out unsigned(7 downto 0);
		minutes		: out unsigned(7 downto 0);
		secpuls		: out std_logic;
		maxrpmM		: out unsigned(7 downto 0);
		maxrpmS		: out unsigned(7 downto 0)
	);
end entity;

architecture rtl of clock is

	signal Ticks 	: unsigned(15 downto 0);
	signal stop, pauze  	: std_logic;														--signaal voor de code om te stoppen wanneer de tijd op 59:59
	signal sec		: unsigned(7 downto 0);
	signal min 		: unsigned(7 downto 0);
	signal sectick		:  std_logic;
	

	
	--dubblestart_stop
type state_type is (start_stop1_1, start_stop0_1, start_stop1_2, start_stop0_2);

	signal next_state , present_state : state_type;
	
begin
	
		combs: process(present_state, start_stop) is
		
		begin
		case present_state is
			when start_stop1_1 =>	
								if start_stop = '1' then
									next_state <= start_stop0_1;
								else
									next_state <= start_stop1_1;
								end if;
								
			when start_stop0_1 => 	
								if start_stop = '0' then
									next_state <= start_stop1_2;
								else
									next_state <= start_stop0_1;
								end if;
								
			when start_stop1_2 => 	
								if start_stop = '1' then
									next_state <= start_stop0_2;
								else
									next_state <= start_stop1_2;
								end if;
								
			when start_stop0_2 =>	
								if start_stop = '0' then
									next_state <= start_stop1_1;
								else
									next_state <= start_stop0_2;
								end if;
								
		end case;
	end process;

	reg: process(clk,reset)	is
	begin
		if reset = '1' then			--reset
			Ticks 	<= "0000000000000000";
			sec 		<= "00000000";
			min 		<= "00000000";
			stop		<= '0';
			sectick	<= '0';
			present_state <= start_stop1_1;
			
		elsif rising_edge(clk) then
		
			present_state <= next_state;
			
			if sysclear = '1' then
				Ticks 	<= "0000000000000000";
				sec 		<= "00000000";
				min 		<= "00000000";
				stop		<= '0';
				sectick	<= '0';
				present_state <= start_stop1_1;
			
			elsif stop = '1' or pauze = '1' then			--stop code
			null;
			
			else
				if Ticks = ClockFrequency then										
					Ticks <= "0000000000000000";
					sectick	<= '1';
					if sec = "00111011" then
						sec <= "00000000";
						min <= min + 1;													--minuut code
					else
						sec <= sec + 1;												--seconde code
					end if;
				else
					sectick	<= '0';
					Ticks <= Ticks + 1;
				end if;
			end if;
			
			
			if sec = "00111010" and min = "00111011" then				--max tijd code
				stop <= '1';
				
				
				if pauze = '1' then												--max tijd code
					Ticks 	<= "0000000000000000";
					sec 		<= "00000000";
					min 		<= "00000000";
					stop 		<= '0';
				end if;
				
				
			end if;
					
		end if;
		
	end process;
	
	process(clk, reset) is
	begin
	if reset = '1' then																	--max rpm code
		maxrpmM <= "00000000";
		maxrpmS <= "00000000";
		
	elsif rising_edge(clk) then
	
		if sysclear = '1' then
			maxrpmM <= "00000000";
			maxrpmS <= "00000000";
		end if; 
		
		if maxflag ='1' then
			maxrpmM <= min;
			maxrpmS <= sec;
			
		else
			null;
			
		end if;
	end if;
		
	end process;

syncout: process (present_state, clk, reset) is
		
		begin
		if reset = '1' then
			pauze <= '1';
		
		elsif rising_edge(clk) then
			if sysclear = '1' then
				pauze <= '1';
			else
			case present_state is
			
				when start_stop1_1 => 
					
					pauze <='1';
				 
				when start_stop0_1 => 
					
					pauze <='0';
					
				when start_stop1_2 => 
				
					pauze <= '0';
					
				when start_stop0_2 => 
					
					pauze <='1';
				
			end case;
			end if;
		end if;
			
	end process;

	secpuls	<= sectick;
	seconds	<= sec;
	minutes	<= min;
	
end architecture;