-- Filename:     tb_lcdcontroller.vhd 
-- Filetype:     VHDL testbench
-- Date:         24/09/2020
-- Description:  A testbench of lcdcontroller
-- Author:       T.westmaas

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity tb_lcdcontroller is
end entity tb_lcdcontroller;

architecture sim of tb_lcdcontroller is
constant Tperiod : time := 100 us;

component lcdcontroller is
	port (CLOCK		: in std_logic;
			areset	: in std_logic;
			--rpm
			rpmmode	: in std_logic_vector(2 downto 0);
			rpmcount	: in std_logic_vector(19 downto 0);
			--paddle resistance 
			paddleres: in std_logic_vector(3 downto 0);
			calready	: in std_logic;
			--tijd
			maxrpmM	: in std_logic_vector(7 downto 0);
			maxrpmS	: in std_logic_vector(7 downto 0);
			min   	: in std_logic_vector(7 downto 0);
			sec   	: in std_logic_vector(7 downto 0);
			-- LCD 
			lcd_gpio_E   : out std_logic;
			lcd_gpio_RS  : out std_logic;
			lcd_gpio_RW  : out std_logic;
			lcd_gpio_D 	 : inout std_logic_vector(7 downto 0)
			
	);
	
end component lcdcontroller;

signal clk, areset, lcd_gpio_E, lcd_gpio_RS, lcd_gpio_RW, calready : std_logic;
signal min, sec, maxrpmM, maxrpmS : std_logic_vector(7 downto 0);
signal lcd_gpio_D : std_logic_vector(7 downto 0);
signal rpmcount : std_logic_vector(15 downto 0);
signal rpmmode : std_logic_vector(1 downto 0);
signal paddleres : std_logic_vector(3 downto 0);


begin

	dut : lcdcontroller
	port map (CLOCK => clk, areset => areset, rpmmode => rpmmode, rpmcount => rpmcount, paddleres => paddleres, calready => calready, min => min, sec => sec, maxrpmM => maxrpmM, maxrpmS => maxrpmS, lcd_gpio_E => lcd_gpio_E, lcd_gpio_RS => lcd_gpio_RS, lcd_gpio_RW => lcd_gpio_RW, lcd_gpio_D => lcd_gpio_D );
	
	
clockgen: process is
	begin
		clk <= '0';
		wait for Tperiod/2;
		clk <= '1';
		wait for Tperiod/2;
	end process clockgen;

		
datagen: process is
	begin
		
		
		--load values
		calready <= '1';
		areset 			<= '0';
		rpmmode	<= "00"; --109
		rpmcount		<= "0000000000111000"; --25.864
		paddleres	<= "0011";--3
		sec <= "00000000";
		min <= "00000000";
		
		wait for 500 ms;
		
		rpmmode	<= "001";
		rpmcount		<= "0000000001101101";
		
		wait for 500 ms;
		
		sec <= "00000001";
		rpmmode	<= "010";
		rpmcount		<= "0000000001111100";
		
		wait for 500 ms;
		
		sec <= "00000010";
		rpmmode	<= "011";
		rpmcount		<= "0110010100001000";
		paddleres	<= "0100";
		
		wait for 400 ms;
		
		paddleres	<= "0101";
		
		wait for 100 ms;
		
		sec <= "00000011";
		rpmmode	<= "000";
		rpmcount		<= "0000000000111000";
		
		wait for 500 ms;
		
		sec <= "00000100";
		rpmmode	<= "001";
		rpmcount		<= "0000000001101101";
		
		
		wait for Tperiod/4;
		
		areset <= '1';
		wait for 2*Tperiod;
		
		rpmmode	<= "000";
		rpmcount		<= "0000000000111000";
		min		<= "00000000"; --00
		sec		<= "00000000"; --00		
		
		areset <= '0';
		wait for 2*Tperiod;
		
		
	end process;
	
end architecture sim;