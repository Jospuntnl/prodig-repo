library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity average is
	port
		(
		areset		:	in	std_logic;
		sysclear 	: 	in std_logic;
		CLOCK_10		:	in	std_logic;
		secpuls   	:	in std_logic;
		rpm			:	in	unsigned(7 downto 0);
		aver			:	out unsigned(7 downto 0)		
	);
end	entity;

architecture rtl	of	average is				
	signal	totalsec	:	unsigned(15 downto 0) 	:= "0000000000000000";
	signal	totalrpm	:	unsigned(19 downto 0) 	:= "00000000000000000000";		--totatale rpm
	signal	end_aver	:	unsigned(19 downto 0)	:= "00000000000000000000";					--de eind waarde van de average
begin
	
	
	
	
	process(CLOCK_10,areset) is
	begin
		if	areset = '1' then
			totalsec <= "0000000000000000";
			totalrpm	<= "00000000000000000000";
			end_aver	<= "00000000000000000000";
			
		elsif	rising_edge(CLOCK_10)	then
		
			if sysclear = '1' then 
				totalsec <= "0000000000000000";
				totalrpm	<= "00000000000000000000";
				end_aver	<= "00000000000000000000";
			end if;
			
			if totalsec = "111000010000" then
				null;
			elsif secpuls = '1' then
				totalsec <= totalsec + 1;
				totalrpm <= totalrpm + rpm;
			end if;
			
			if totalsec = "0000000000000000" then
			end_aver <= "00000000000000000000";
			else
			end_aver <= totalrpm / totalsec;
			end if;
			
		end if;
		
		
		
	end process;
	aver<= end_aver(7 downto 0);
end architecture;	
	