--Datum: 9-10-20
--Auteur: Jos Knol & Hao Zheng & Terry Westmaas
--Beschrijving:
--		Structural beschrijving van het hometrainer project.
--Versie: 1.0


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


-- define de entity met alle i/o
entity Hometrainer	is 

	port (
			--debug singaalen
			resetled		: out std_logic;
			
			--start
			areset		: in std_logic;
			CLOCK_50		: in std_logic;
			--GPIO
			--Hallsensor input
			Hallsensor	: in std_logic;
			--LCD 
			lcddata		: inout std_logic_vector(7 downto 0);
			lcdE			: out std_logic;
			lcdRW			: out std_logic;
			lcdRS			: out std_logic;
			--trapweerstand 
			--motor
			Motorp		: out std_logic;
			Motorn		: out std_logic;
			--trapweerstand positie 
			adcdata		: in unsigned(7 downto 0);
			adcbusy		: in std_logic;
			adcstart		: out std_logic;
			adcRD			: out std_logic;
			--gebruiker knoppen
			knop1			: in std_logic;
			knop2			: in std_logic;
			knop3			: in std_logic;
			knop4			: in std_logic;
			knop5			: in std_logic;
			knop6			: in std_logic
		  );
end entity Hometrainer;

architecture structural of Hometrainer is

-- Hier voegen we alle structural componenten toe
--	de klok prescaler
component prescaler is
	port (clkin  : in std_logic;
			areset : in std_logic;
			clkout : out std_logic
		  );
end component prescaler;

-- rpmsensor - bepaald de rpm
component rpmsens is
	port (clk 			: in std_logic;
			areset 		: in std_logic;
			sysclear		: in std_logic;
			hallsensor	: in std_logic;
			rpm			: out unsigned(7 downto 0);
			rotations	: out unsigned(15 downto 0);
			max_rpm		: out unsigned(7 downto 0);
			maxflag		: out std_logic
		  );
end component rpmsens;

-- klok - meet de verstreken tijd
component clock is
	port
	(
		clk 			: in	std_logic;
		sysclear		: in 	std_logic;
		reset			: in	std_logic;
		start_stop	: in	std_logic;
		maxflag		: in 	std_logic;
		seconds		: out unsigned(7 downto 0);
		minutes		: out unsigned(7 downto 0);
		secpuls		: out std_logic;
		maxrpmM		: out unsigned(7 downto 0);
		maxrpmS		: out unsigned(7 downto 0)
	);
end component clock;

-- bepaald het gemiddelde RPM
component average is
	port
			(
		areset		:	in	std_logic;
		sysclear 	: 	in std_logic;
		CLOCK_10		:	in	std_logic;
		secpuls   	:	in std_logic;
		rpm			:	in	unsigned(7 downto 0);
		aver			:	out unsigned(7 downto 0)		
	);
end component;

-- trap weerstand regelaar
component trapweerstand is
	port (
			sysclear			: in std_logic;
			out_measured	: out std_logic_vector(7 downto 0);
			clk				: in std_logic;
			input_up			: in std_logic;
			input_down		: in std_logic;
			in_adc			: in unsigned(7 downto 0);
			calready			: out std_logic;
			paddleres		: out unsigned(3 downto 0);
			motor_up			: out std_logic;
			motor_down		: out std_logic;
			busy				: in std_logic;
			readdata			: out std_logic;
			areset			: in std_logic;
			convstart		: out std_logic
		  );
			
end component;

-- LCD bestuuring
component Displaydriver is

	port (CLOCK_10 : in std_logic;
			areset	: in std_logic;
			--trapweerstand
			paddleres  : in std_logic_vector(3 downto 0);
			calready	  : in std_logic;
			--RPM 
			modebutton	: in std_logic;
			huidigrpm	: in std_logic_vector(7 downto 0);
			maxrpm   	: in std_logic_vector(7 downto 0);
			gemrpm   	: in std_logic_vector(7 downto 0);
			omwcount 	: in std_logic_vector(15 downto 0);
			adcdata		: in std_logic_vector(7 downto 0);
			--tijd
			maxrpmM	: in std_logic_vector(7 downto 0);
			maxrpmS	: in std_logic_vector(7 downto 0);
			min   	: in std_logic_vector(7 downto 0);
			sec   	: in std_logic_vector(7 downto 0);
			-- LCD outputs
			lcd_gpio_E   : out std_logic;
			lcd_gpio_RS  : out std_logic;
			lcd_gpio_RW  : out std_logic;
			lcd_gpio_D 	 : inout std_logic_vector(7 downto 0)
			
		  );
end component Displaydriver;

-- ontdenderaar van de gebruikers knoppen
component debouncer is

	port (CLOCK_10 : in std_logic;
			--knoppen
			knop1			: in std_logic;--mode
			knop2			: in std_logic;--start/stop
			knop3			: in std_logic;--recovery
			knop4			: in std_logic;--up
			knop5			: in std_logic;--reset
			knop6			: in std_logic;--down
			areset		: in std_logic;--
			
			systemreset		: out std_logic;
			modebutton  	: out std_logic;
			start_stop  	: out std_logic;		
			--Recovery  	: out std_logic;		Niet gebruikt
			input_up 	 	: out std_logic;
			input_down		: out std_logic;
			
			resetled     	: out std_logic
		  );
end component debouncer;


-- Alle interne verbindingssignalen.

signal clock_10		: std_logic;						--Systeemklok
signal rpmdata		: unsigned(7 downto 0);				--Rpm signaal
signal maxrpm		: unsigned(7 downto 0);				--maxrpm
signal rotations 	: unsigned(15 downto 0);			--Aantal omwentelingen
signal seconden	: unsigned(7 downto 0);				--Aantal seconden
signal minuten		: unsigned(7 downto 0);				--Aantal minuten
signal secpuls		: std_logic;								--Clock puls
signal calready_2_calready		: std_logic;
signal gemiddelde	: unsigned(7 downto 0);				--Gemiddelde rpm
signal paddleres	: unsigned(3 downto 0);				--Hoe zwaar staat 
signal maxflag		: std_logic;							--maxflag
signal maxrpmM, maxrpmS	: unsigned(7 downto 0);		--maxrpm minuten en seconden
signal sysclear	: std_logic;							--systemclear
signal adcmeasures: std_logic_vector(7 downto 0);	--gemeten adcdata

signal modebutton,  input_up, input_down, start_stop : std_logic; --knopsingalen vanuit ontdenderaar start_stop, Recovery,
--signal Recovery : std_logic;

begin
		--Alle componenten
		u0: prescaler
		port map (clkin => CLOCK_50, clkout => clock_10, areset => areset);
		
		u1: rpmsens
		port map (areset => areset, clk => clock_10, rpm => rpmdata, rotations => rotations, hallsensor => Hallsensor, max_rpm => maxrpm, maxflag => maxflag, sysclear => sysclear);
		
		u2: clock
		port map(clk => clock_10, sysclear => sysclear, reset => areset, start_stop => start_stop, seconds => seconden, minutes => minuten, secpuls => secpuls, maxrpmM => maxrpmM, maxrpmS => maxrpmS, maxflag => maxflag );
		
		u3: average
		port map(areset => areset, sysclear => sysclear, CLOCK_10 => CLOCK_10, rpm => rpmdata, secpuls => secpuls, aver => gemiddelde);	

		u4: trapweerstand
		port map(areset => areset, out_measured => adcmeasures, sysclear => sysclear, clk => clock_10, input_up => input_up, input_down => input_down, in_adc => adcdata, calready => calready_2_calready, paddleres => paddleres, motor_up => Motorp, motor_down => Motorn, busy => adcbusy, readdata => adcRD, convstart => adcstart);
		
		u5: Displaydriver
		port map(CLOCK_10 => clock_10, adcdata => adcmeasures, areset => areset, paddleres	=> std_logic_vector(paddleres), modebutton => modebutton, huidigrpm => std_logic_vector(rpmdata), maxrpm => std_logic_vector(maxrpm), gemrpm => std_logic_vector(gemiddelde), omwcount => std_logic_vector(rotations), min => std_logic_vector(minuten), sec => std_logic_vector(seconden), lcd_gpio_E => lcdE, lcd_gpio_RS => lcdRS, lcd_gpio_RW => lcdRW, lcd_gpio_D => lcddata, maxrpmM => std_logic_vector(maxrpmM), maxrpmS => std_logic_vector(maxrpmS), calready => calready_2_calready );
		
		u6: debouncer
		port map(CLOCK_10 => clock_10, knop1 => knop1, knop2 => knop2, knop3 => knop3, knop4 => knop4, knop5 => knop5, knop6 => knop6, areset => areset, systemreset => sysclear, modebutton => modebutton,  input_up => input_up, input_down => input_down, resetled => resetled, start_stop => start_stop );
					--, Recovery => Recovery, );
					
		
end structural;
	