--Datum: 9-10-20
--Auteur: Jos Knol
--Beschrijving:
--		Een look-up-table om teller-waarden om te zetten naar bruikbare rpm waarden
--Versie: 1.1


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- define de entity deler
entity deler is
	port(
			countval		: in unsigned(31 downto 0);
			sysclear		: in std_logic;
			rpmo			: out unsigned(7 downto 0);
			areset		: in std_logic;
			clk			: in std_logic;
			maxrpm		: out unsigned(7 downto 0);
			maxflag		: out std_logic
		);
end entity deler;

architecture behave of deler is
signal max_rpm			: unsigned(7 downto 0);
signal rpm				: unsigned(7 downto 0);

begin

process(clk, areset) is
-- Maak een variabele voor de look-up-table (LUT).
variable waarde 		: integer;
begin
-- Reset functie.
if areset = '1' then
	rpm <= "00000000";

elsif rising_edge(clk) then
	if sysclear ='1' then
		rpm <= "00000000";
		
	else
-- De LUT, met alle tellerwaarden, dit vanwege hardware-besparing.
waarde := to_integer(countval);
case waarde is
when 29984 to 30016    => rpm <= "00010100";
when 29221 to 29983    => rpm <= "00010101";
when 27866 to 29220    => rpm <= "00010110";
when 26630 to 27865    => rpm <= "00010111";
when 25500 to 26629    => rpm <= "00011000";
when 24462 to 25499    => rpm <= "00011001";
when 23504 to 24461    => rpm <= "00011010";
when 22619 to 23503    => rpm <= "00011011";
when 21798 to 22618    => rpm <= "00011100";
when 21034 to 21797    => rpm <= "00011101";
when 20323 to 21033    => rpm <= "00011110";
when 19657 to 20322    => rpm <= "00011111";
when 19034 to 19656    => rpm <= "00100000";
when 18449 to 19033    => rpm <= "00100001";
when 17899 to 18448    => rpm <= "00100010";
when 17381 to 17898    => rpm <= "00100011";
when 16892 to 17380    => rpm <= "00100100";
when 16430 to 16891    => rpm <= "00100101";
when 15992 to 16429    => rpm <= "00100110";
when 15577 to 15991    => rpm <= "00100111";
when 15183 to 15576    => rpm <= "00101000";
when 14808 to 15182    => rpm <= "00101001";
when 14452 to 14807    => rpm <= "00101010";
when 14112 to 14451    => rpm <= "00101011";
when 13788 to 14111    => rpm <= "00101100";
when 13478 to 13787    => rpm <= "00101101";
when 13182 to 13477    => rpm <= "00101110";
when 12899 to 13181    => rpm <= "00101111";
when 12628 to 12898    => rpm <= "00110000";
when 12367 to 12627    => rpm <= "00110001";
when 12118 to 12366    => rpm <= "00110010";
when 11878 to 12117    => rpm <= "00110011";
when 11647 to 11877    => rpm <= "00110100";
when 11426 to 11646    => rpm <= "00110101";
when 11212 to 11425    => rpm <= "00110110";
when 11006 to 11211    => rpm <= "00110111";
when 10808 to 11005    => rpm <= "00111000";
when 10617 to 10807    => rpm <= "00111001";
when 10432 to 10616    => rpm <= "00111010";
when 10254 to 10431    => rpm <= "00111011";
when 10082 to 10253    => rpm <= "00111100";
when 9915 to 10081    => rpm <= "00111101";
when 9754 to 9914    => rpm <= "00111110";
when 9598 to 9753    => rpm <= "00111111";
when 9447 to 9597    => rpm <= "01000000";
when 9301 to 9446    => rpm <= "01000001";
when 9159 to 9300    => rpm <= "01000010";
when 9021 to 9158    => rpm <= "01000011";
when 8887 to 9020    => rpm <= "01000100";
when 8758 to 8886    => rpm <= "01000101";
when 8632 to 8757    => rpm <= "01000110";
when 8509 to 8631    => rpm <= "01000111";
when 8390 to 8508    => rpm <= "01001000";
when 8275 to 8389    => rpm <= "01001001";
when 8162 to 8274    => rpm <= "01001010";
when 8053 to 8161    => rpm <= "01001011";
when 7946 to 8052    => rpm <= "01001100";
when 7842 to 7945    => rpm <= "01001101";
when 7741 to 7841    => rpm <= "01001110";
when 7642 to 7740    => rpm <= "01001111";
when 7546 to 7641    => rpm <= "01010000";
when 7453 to 7545    => rpm <= "01010001";
when 7361 to 7452    => rpm <= "01010010";
when 7272 to 7360    => rpm <= "01010011";
when 7185 to 7271    => rpm <= "01010100";
when 7100 to 7184    => rpm <= "01010101";
when 7017 to 7099    => rpm <= "01010110";
when 6936 to 7016    => rpm <= "01010111";
when 6856 to 6935    => rpm <= "01011000";
when 6779 to 6855    => rpm <= "01011001";
when 6703 to 6778    => rpm <= "01011010";
when 6629 to 6702    => rpm <= "01011011";
when 6557 to 6628    => rpm <= "01011100";
when 6486 to 6556    => rpm <= "01011101";
when 6417 to 6485    => rpm <= "01011110";
when 6349 to 6416    => rpm <= "01011111";
when 6282 to 6348    => rpm <= "01100000";
when 6217 to 6281    => rpm <= "01100001";
when 6153 to 6216    => rpm <= "01100010";
when 6091 to 6152    => rpm <= "01100011";
when 6030 to 6090    => rpm <= "01100100";
when 5970 to 6029    => rpm <= "01100101";
when 5911 to 5969    => rpm <= "01100110";
when 5853 to 5910    => rpm <= "01100111";
when 5797 to 5852    => rpm <= "01101000";
when 5741 to 5796    => rpm <= "01101001";
when 5687 to 5740    => rpm <= "01101010";
when 5633 to 5686    => rpm <= "01101011";
when 5581 to 5632    => rpm <= "01101100";
when 5530 to 5580    => rpm <= "01101101";
when 5479 to 5529    => rpm <= "01101110";
when 5430 to 5478    => rpm <= "01101111";
when 5381 to 5429    => rpm <= "01110000";
when 5333 to 5380    => rpm <= "01110001";
when 5286 to 5332    => rpm <= "01110010";
when 5240 to 5285    => rpm <= "01110011";
when 5195 to 5239    => rpm <= "01110100";
when 5150 to 5194    => rpm <= "01110101";
when 5106 to 5149    => rpm <= "01110110";
when 5063 to 5105    => rpm <= "01110111";
when 5021 to 5062    => rpm <= "01111000";
when 4979 to 5020    => rpm <= "01111001";
when 4938 to 4978    => rpm <= "01111010";
when 4898 to 4937    => rpm <= "01111011";
when 4858 to 4897    => rpm <= "01111100";
when 4819 to 4857    => rpm <= "01111101";
when 4781 to 4818    => rpm <= "01111110";
when 4743 to 4780    => rpm <= "01111111";
when 4706 to 4742    => rpm <= "10000000";
when 4669 to 4705    => rpm <= "10000001";
when 4633 to 4668    => rpm <= "10000010";
when 4598 to 4632    => rpm <= "10000011";
when 4563 to 4597    => rpm <= "10000100";
when 4528 to 4562    => rpm <= "10000101";
when 4494 to 4527    => rpm <= "10000110";
when 4461 to 4493    => rpm <= "10000111";
when 4428 to 4460    => rpm <= "10001000";
when 4395 to 4427    => rpm <= "10001001";
when 4363 to 4394    => rpm <= "10001010";
when 4332 to 4362    => rpm <= "10001011";
when 4301 to 4331    => rpm <= "10001100";
when 4270 to 4300    => rpm <= "10001101";
when 4240 to 4269    => rpm <= "10001110";
when 4210 to 4239    => rpm <= "10001111";
when 4181 to 4209    => rpm <= "10010000";
when 4152 to 4180    => rpm <= "10010001";
when 4124 to 4151    => rpm <= "10010010";
when 4095 to 4123    => rpm <= "10010011";
when 4068 to 4094    => rpm <= "10010100";
when 4040 to 4067    => rpm <= "10010101";
when 4013 to 4039    => rpm <= "10010110";
when 3987 to 4012    => rpm <= "10010111";
when 3960 to 3986    => rpm <= "10011000";
when 3934 to 3959    => rpm <= "10011001";
when 3909 to 3933    => rpm <= "10011010";
when 3883 to 3908    => rpm <= "10011011";
when 3858 to 3882    => rpm <= "10011100";
when 3834 to 3857    => rpm <= "10011101";
when 3809 to 3833    => rpm <= "10011110";
when 3785 to 3808    => rpm <= "10011111";
when 3762 to 3784    => rpm <= "10100000";
when 3738 to 3761    => rpm <= "10100001";
when 3715 to 3737    => rpm <= "10100010";
when 3692 to 3714    => rpm <= "10100011";
when 3670 to 3691    => rpm <= "10100100";
when 3647 to 3669    => rpm <= "10100101";
when 3625 to 3646    => rpm <= "10100110";
when 3604 to 3624    => rpm <= "10100111";
when 3582 to 3603    => rpm <= "10101000";
when 3561 to 3581    => rpm <= "10101001";
when 3540 to 3560    => rpm <= "10101010";
when 3519 to 3539    => rpm <= "10101011";
when 3498 to 3518    => rpm <= "10101100";
when 3478 to 3497    => rpm <= "10101101";
when 3458 to 3477    => rpm <= "10101110";
when 3438 to 3457    => rpm <= "10101111";
when 3419 to 3437    => rpm <= "10110000";
when 3399 to 3418    => rpm <= "10110001";
when 3380 to 3398    => rpm <= "10110010";
when 3361 to 3379    => rpm <= "10110011";
when 3343 to 3360    => rpm <= "10110100";
when 3324 to 3342    => rpm <= "10110101";
when 3306 to 3323    => rpm <= "10110110";
when 3288 to 3305    => rpm <= "10110111";
when 3270 to 3287    => rpm <= "10111000";
when 3252 to 3269    => rpm <= "10111001";
when 3234 to 3251    => rpm <= "10111010";
when 3217 to 3233    => rpm <= "10111011";
when 3200 to 3216    => rpm <= "10111100";
when 3183 to 3199    => rpm <= "10111101";
when 3166 to 3182    => rpm <= "10111110";
when 3150 to 3165    => rpm <= "10111111";
when 3133 to 3149    => rpm <= "11000000";
when 3117 to 3132    => rpm <= "11000001";
when 3101 to 3116    => rpm <= "11000010";
when 3085 to 3100    => rpm <= "11000011";
when 3069 to 3084    => rpm <= "11000100";
when 3053 to 3068    => rpm <= "11000101";
when 3038 to 3052    => rpm <= "11000110";
when 3023 to 3037    => rpm <= "11000111";
when 3007 to 3022    => rpm <= "11001000";

-- Je bent ver gekomen als je hier nog steeds leest.... Zo interresant is het nou ook weer niet.

-- Als de tellerwaarde niet gedefiniëerd is, wordt de RPM 0.
when others				=> rpm <= "00000000";

end case;
end if;

end if;
end process;

rpmo <= rpm;

--process voor max rpm

process(clk, areset) is
begin
	if areset = '1' then
		max_rpm <= "00000000";
	elsif rising_edge(clk) then
		if sysclear ='1' then
			max_rpm <= "00000000";
			
		end if; 
	--Kijk of de huidige waarde groter is, zoja, vervang hem.
		if rpm > max_rpm then
			max_rpm <= rpm;
			maxflag <= '1';
			
		else
		maxflag <= '0';
		
		end if;
	end if;
end process;

maxrpm <= max_rpm;

end behave;
