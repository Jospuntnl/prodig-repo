-- Author: Ruben La Lau
-- Student electrical engineering at The Hague University
-- Date: 12-10-2020

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_trapweerstandselector is
end entity tb_trapweerstandselector;

architecture sim of tb_trapweerstandselector is

	
component trapweerstandselector is
	port (	
			adc_up		: in std_logic;
			adc_down		: in std_logic;
			ready			: in std_logic;
			sysclear		: in std_logic;
			clk			: in std_logic;
			areset		: in std_logic;
			paddleres	: out unsigned(3 downto 0);
			input_up		: in std_logic;
			input_down	: in std_logic;
			in_adc		: in unsigned (3 downto 0);
			out_up		: out std_logic;
			out_down		: out std_logic
			);
end component trapweerstandselector;

	signal clk : std_logic;
	signal areset : std_logic;
	signal input_up : std_logic;
	signal input_down : std_logic;
	signal in_adc: unsigned(7 downto 0);
	signal out_up : std_logic;
	signal out_down : std_logic;
	signal paddleres : unsigned(3 downto 0);
	signal adc_up		:  std_logic;
	signal adc_down		: std_logic;
	signal ready			: std_logic;
	signal sysclear		: std_logic;
		
begin
	dut : trapweerstandselector
	port map (clk => clk, areset => areset, paddleres => paddleres, input_up => input_up, input_down => input_down, in_adc => in_adc, out_up => out_up, out_down => out_down, adc_up => adc_up, adc_down => adc_down, ready => ready, sysclear => sysclear);
	
	clockgen : process is
	begin
		clk <= '0';
		wait for 50 ns;
		clk <= '1';
		wait for 50 ns;
	end process clockgen;
	
	datagen : process is
	begin
		ready <= '0';
	
		adc_up  <= '1';
		adc_down <= '0';
		
		wait for 500 ns;
		
		adc_up  <= '0';
		adc_down <= '1';
		
		wait for 500 ns;
		
		ready <= '1';
		
		input_up <= '0';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '1';
		wait until clk = '1';
		
		wait for 5 ns;
		
		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';
		
		wait for 5 ns;
		
		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;

		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;

		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;

		input_up <= '0';
		input_down <= '1';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '0';
		input_down <= '1';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '0';
		input_down <= '1';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '0';
		input_down <= '1';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '0';
		input_down <= '1';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '0';
		input_down <= '1';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '0';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '1';
		wait until clk = '1';

		wait for 5 ns;
		
		wait;
		
	end process datagen;
	
end sim;