-- Filename:  rpmselector.vhd   
-- Filetype:  vhdl
-- Date:         24/09/2020
-- Description:   
-- Author:     E. Heijerman


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rpmselector is
    port (clk     : in std_logic;
			areset	: in std_logic;
			mode	   : in std_logic;
			huidigrpm: in std_logic_vector(7 downto 0);
			maxrpm   : in std_logic_vector(7 downto 0);
			gemrpm   : in std_logic_vector(7 downto 0);
			omwcount : in std_logic_vector(15 downto 0);
			adcdata	: in std_logic_vector(7 downto 0);
			rpmmode	: out std_logic_vector(2 downto 0);
			rpmcount : out std_logic_vector(15 downto 0)
         );
end entity rpmselector;

architecture Code of rpmselector is

-- State types
type state_type is (s0, s1, s2, s3, s4, s5, s6, s7, s8, s9);

-- Internal current state and next state;
signal rpmmodus : std_logic_vector(2 downto 0) := "000";
signal current_state, next_state : state_type;
begin



	-- Combinational process generates the next state
	combstate: process (current_state, mode) is
	begin
		case current_state is
			
			when s0 =>	if mode = '1' then
								next_state <= s1;
							else
								next_state <= s0;
							end if;
							
			when s1 =>	if mode = '0' then
								next_state <= s2;
							else
								next_state <= s1;
							end if;
							
			when s2 =>	if mode = '1' then
								next_state <= s3;
							else
								next_state <= s2;
							end if;
							
			when s3 =>	if mode = '0' then
								next_state <= s4;
							else
								next_state <= s3;
							end if;
							
			when s4 =>	if mode = '1' then
								next_state <= s5;
							else
								next_state <= s4;
							end if;
							
			when s5 =>	if mode = '0' then
								next_state <= s6;
							else
								next_state <= s5;
							end if;
							
			when s6 =>	if mode = '1' then
								next_state <= s7;
							else
								next_state <= s6;
							end if;
							
			when s7 =>	if mode = '0' then
								next_state <= s8;
							else
								next_state <= s7;
							end if;
							
			when s8 => 	if mode = '1' then
								next_state <= s9;
							else 
								next_state <= s8;
							end if;
							
			when s9 =>	if mode = '0' then
								next_state <= s0;
							else
								next_state <= s9;
							end if;
							
			when others => next_state <= s0;
		end case;
	end process;

	-- Sequential process generates the state register
	reg: process (clk, areset) is
	begin
		if areset = '1' then
			current_state <= s0;
		elsif rising_edge(clk) then
			current_state <= next_state;
		end if;
	end process;

	-- Combinational process generates the output
	combout: process (current_state) is
	begin
		case current_state is
			when s0 =>	rpmmodus	 <= "000";
							
			when s1 =>	rpmmodus	 <= "000";
							
			when s2 =>	rpmmodus	 <= "001";
							
			when s3 =>	rpmmodus	 <= "001";
							
			when s4 =>	rpmmodus	 <= "010";
							
			when s5 =>	rpmmodus	 <= "010";
							
			when s6 =>	rpmmodus	 <= "011";
							
			when s7 =>	rpmmodus	 <= "011";
			
			when s8 =>	rpmmodus	 <= "111";
			
			when s9 =>	rpmmodus	 <= "111";
							
			when others => rpmmodus	 <= "000";
		end case;
	end process;
	
	rpmmode <= rpmmodus;
	
	rpmcount  <= "00000000" & huidigrpm when rpmmodus = "000" else
					 "00000000" & maxrpm 	when rpmmodus = "001" else
					 "00000000" & gemrpm 	when rpmmodus = "010" else
					 "00000000" & adcdata   when rpmmodus = "111" else
					 omwcount 					when rpmmodus = "011" else
					 
					 "0000000000000000";
	
	
end architecture;