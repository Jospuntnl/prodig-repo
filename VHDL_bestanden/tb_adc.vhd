library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity tb_adc is
end entity tb_adc;

architecture sim of tb_adc is
component adc is
	port( clk				: in std_logic;
			areset			: in std_logic;
			busy				: in std_logic;
			data				: in unsigned(7 downto 0);
			readdata			: out std_logic;
			dataout			: out unsigned(7 downto 0);
			convstart		: out std_logic;
			ready				: out std_logic;
			out_up			: out std_logic;
			out_down			: out std_logic
	);
end component adc;

signal	clk				: std_logic;
signal	areset			: std_logic;
signal	busy				: std_logic;
signal	data				: unsigned(7 downto 0);
signal	readdata			: std_logic;
signal	dataout			: unsigned(7 downto 0);
signal	convstart		: std_logic;
signal	ready				: std_logic;
signal	out_up			: std_logic;
signal	out_down			: std_logic;

begin
	dut: adc
	port map(clk => clk, areset => areset, busy => busy, data => data, readdata => readdata, dataout => dataout, convstart => convstart, ready => ready, out_up => out_up, out_down => out_down);
	
		clockgen : process is
	begin
		clk <= '0';
		wait for 50 ns;
		clk <= '1';
		wait for 50 ns;
	end process clockgen;
	
	datagen: process is
	begin
	areset <= '1';
	wait for 50 us;
	areset <= '0';
	wait for 50 us;
	busy <= '1';
	data <= "11111111";
	wait for 100 us;
	busy <= '0';
	wait for 50 us;
	busy <= '1';
	data <= "00000000";
	wait for 100 us;
	busy <= '0';
	wait;
	end process datagen;
end sim;