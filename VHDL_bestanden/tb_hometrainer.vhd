--Datum: 16-10-20
--Auteur: Jos Knol
--Beschrijving:
--		Een testbench voor het structurele hometrainer project
-- Versie: 1.0

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_Hometrainer is
end entity;

architecture sim of tb_Hometrainer is

--Dit is een testbench voor de top-level, wij zouden voor alle mogelijke combinaties data kunnen genereren, 
--maar dat is uiteraard gewoon onzin, het is beter om dat voor elk deelsysteem zelf te bekijken.
--In deze testbench testen wij daarom de resetknoppen, de prescaler en de eerste seconde van de klok. We laten alleen de 1e seconde van de klok zien omdat modelsim er anders 10 minuten over doet.


component Hometrainer is
	port (
			areset		: in std_logic;
			CLOCK_50		: in std_logic;
			Hallsensor	: in std_logic;
			lcddata		: inout std_logic_vector(7 downto 0);
			lcdE			: out std_logic;
			lcdRW			: out std_logic;
			lcdRS			: out std_logic;
			Motorp		: out std_logic;
			Motorn		: out std_logic;
			adcdata		: in unsigned(7 downto 0);
			adcbusy		: in std_logic;
			adcstart		: out std_logic;
			adcRD			: out std_logic;
			knop1			: in std_logic;
			knop2			: in std_logic;
			knop3			: in std_logic;
			knop4			: in std_logic;
			knop5			: in std_logic;
			knop6			: in std_logic
		  );
end component Hometrainer;


signal			areset		:  std_logic;
signal			CLOCK_50		:  std_logic;
signal			Hallsensor	:  std_logic;
signal			lcddata		:  std_logic_vector(7 downto 0);
signal			lcdE			:  std_logic;
signal			lcdRW			:  std_logic;
signal			lcdRS			:  std_logic;
signal			Motorp		:  std_logic;
signal			Motorn		:  std_logic;
signal			adcdata		:  unsigned(7 downto 0);
signal			adcbusy		:  std_logic;
signal			adcstart		:  std_logic;
signal			adcRD			:  std_logic;
signal			knop1			:  std_logic;
signal			knop2			:  std_logic;
signal			knop3			:  std_logic;
signal			knop4			:  std_logic;
signal			knop5			:  std_logic;
signal			knop6			:  std_logic;
signal 			sysclk		: std_logic;							--Systeemklok
signal			rpmdata		: unsigned(7 downto 0);				--Rpm signaal
signal 			maxrpm		: unsigned(7 downto 0);				--maxrpm
signal 			rotations 	: unsigned(15 downto 0);			--Aantal omwentelingen
signal 			seconden		: unsigned(7 downto 0);				--Aantal seconden
signal 			minuten		: unsigned(7 downto 0);				--Aantal minuten
signal 			tikkie		: unsigned(15 downto 0);			--Clock puls teller
signal 			gemiddelde	: unsigned(7 downto 0);				--Gemiddelde rpm
signal 			paddleres	: unsigned(3 downto 0);				--Hoe zwaar staat 
signal 			maxflag		: std_logic;							--maxflag
signal 			maxrpmM, maxrpmS	: unsigned(7 downto 0);		--maxrpm minuten en seconden
signal modebutton,  input_up, reset, input_down : std_logic; --knopsingalen vanuit ontdenderaar start_stop, Recovery,
--signal start_stop, Recovery, : std_logic;

begin
	dut: Hometrainer
	port map(areset => areset, CLOCK_50 => CLOCK_50, Hallsensor => Hallsensor, lcddata => lcddata, lcdE => lcdE, lcdRS => lcdRS, lcdRW => lcdRW, Motorn => Motorn, Motorp => Motorp, adcdata => adcdata,
				adcbusy => adcbusy, adcstart => adcstart, adcRD => adcRD, knop1 => knop1, knop2 => knop2, knop3 => knop3, knop4 => knop4, knop5 => knop5, knop6 => knop6);
				
	clockgen: process is
	begin
	
		CLOCK_50 <= '0';
		
		wait for 20 ns;
		
		loop
			CLOCK_50 <= '1';
			
			wait for 10 ns;
			
			CLOCK_50 <= '0';
			
			wait for 10 ns;
		end loop;
	end process;
	
	datagen: process is
	begin
	
		areset <= '1';
		wait for 5 us;
		areset <= '0';
		knop5 <= '0';
		wait for 100 us;
		knop5 <= '1';
		wait for 5000 us;
		knop5 <= '0';
		wait for 5000 us;
		knop5 <= '1';
		wait for 5000 us;
		knop5 <= '0';
		wait;
		
		
	end process;
end architecture;