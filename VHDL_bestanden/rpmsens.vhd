--Datum: 9-10-20
--Auteur: Jos Knol
--Beschrijving:
--		Een structural beschrijving om het aantal klokpulsen tussen twee ingangspulsen te meten, 
--		en daarna om te zetten naar een bruikbare RPM waarde.
-- Versie: 1.1

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--Define de entity.

entity rpmsens	is 

	port (clk 			: in std_logic;
			areset	: in std_logic;
			hallsensor	: in std_logic;
			rpm			: out unsigned(7 downto 0);
			rotations	: out unsigned(15 downto 0);
			max_rpm		: out unsigned(7 downto 0);
			maxflag		: out std_logic;
			sysclear		: in std_logic
		  );
end entity rpmsens;

architecture structural of rpmsens is

-- Alle componenten met port maps.

component teller is
	port(
		
		sysclear		: in std_logic;
		hallsens		: in std_logic;
		counter		: out unsigned(31 downto 0);
		areset		: in std_logic;
		clk			: in std_logic;
		rotations	: out unsigned(15 downto 0)
		);
end component teller;


component deler is
	port(
			sysclear		: in std_logic;
			countval		: in unsigned(31 downto 0);
			rpmo			: out unsigned(7 downto 0);
			areset		: in std_logic;
			maxrpm		: out unsigned(7 downto 0);
			maxflag		: out std_logic;
			clk			: in std_logic);
end component deler;

-- Er is maar 1 intern signaal nodig.
signal rpmcount 	: unsigned(31 downto 0);

begin

-- De portmaps van de componenten
		
		u0: teller
		port map (clk => clk, hallsens => hallsensor, areset => areset, counter => rpmcount, rotations => rotations, sysclear => sysclear);
		
	
		u1: deler
		port map(countval => rpmcount, rpmo => rpm, areset => areset, clk => clk, maxrpm => max_rpm, maxflag => maxflag, sysclear => sysclear);
		
		
end structural;
	