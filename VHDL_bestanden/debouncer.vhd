-- Filename: 	debouncer.vhd   
-- Filetype: 	vhdl
-- Date:         10/10/2020
-- Description:  VHDL Description 
-- Author:       T.westmaas

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity debouncer is

	port (CLOCK_10 : in std_logic;
			--knoppen
			knop1			: in std_logic;--up
			knop2			: in std_logic;--mode
			knop3			: in std_logic;--start/stop
			knop4			: in std_logic;--recovery
			knop5			: in std_logic;--down
			knop6			: in std_logic;--systemreset
			areset		: in std_logic;--
			
			--outputs
			systemreset		: out std_logic;
			modebutton  	: out std_logic;
			start_stop  	: out std_logic;
--			Recovery  		: out std_logic;
			input_up 	 	: out std_logic;
			input_down		: out std_logic;
			
			resetled     	: out std_logic
		  );
end entity debouncer;

architecture deb of debouncer is

signal knop1_0, knop1_1, knop1_2, knop1_3, knop1_4, knop1_5, knop1_out	 : std_logic := '0'; --knop1_
signal knop2_0, knop2_1, knop2_2, knop2_out 										 : std_logic := '0'; --knop2_
signal knop3_0, knop3_1, knop3_2, knop3_out										 : std_logic := '0'; --knop3_
--signal knop4_0, knop4_1, knop4_2, knop4_out										 : std_logic := '0'; --knop4_
signal knop5_0, knop5_1, knop5_2, knop5_3, knop5_4, knop5_5, knop5_out 	 : std_logic := '0'; --knop5_
signal knop6_0, knop6_1, knop6_2, knop6_out										 : std_logic := '0'; --knop6_
signal knop: std_logic;

--dubblesystemreset
signal startdubblereset : std_logic;
signal setdubblereset : std_logic;

type state_type is (systemreset1_1, systemreset0_1, systemreset1_2, systemreset0_2, systemresetdelay);

signal next_state , present_state : state_type;

begin

	combs: process(CLOCK_10, areset, present_state, knop, setdubblereset) is
		begin
		
		if areset = '1' then
		
		startdubblereset <= '0';
		
		elsif rising_edge(CLOCK_10) then
			case present_state is
				when systemreset1_1 =>	if knop = '1' then
													next_state <= systemreset0_1;
												else
													next_state <= systemreset1_1;
												end if;
									
				when systemreset0_1 => 	if knop = '0' then
													next_state <= systemreset1_2;
													startdubblereset <= '1';
												else
													next_state <= systemreset0_1;
												end if;
									
				when systemreset1_2 => 	
												if knop = '1' then
													startdubblereset <= '0';
													next_state <= systemreset0_2;
												elsif setdubblereset = '1' then -- 2,5 seconden
													startdubblereset <= '0';
													next_state <= systemreset1_1;
												else
													next_state <= systemreset1_2;
												end if;
												
				when systemreset0_2 =>	if knop = '0' then
													next_state <= systemresetdelay;
												else
													next_state <= systemreset0_2;
												end if;
									
				when systemresetdelay =>	next_state <= systemreset1_1;
									
				when others => next_state <= present_state;
			end case;
			end if;
	end process;
	

 reg: process (CLOCK_10, areset, startdubblereset) is
variable dubblereset :	unsigned(15 downto 0);
	begin

	if areset = '1' then
	
		knop1_0 <= '0'; knop1_1 <= '0'; knop1_2 <= '0'; knop1_3 <= '0'; knop1_4 <= '0'; knop1_5 <= '0'; knop1_out <= '0';
		knop2_0 <= '0'; knop2_1 <= '0'; knop2_2 <= '0'; knop2_out <= '0';
		knop3_0 <= '0'; knop3_1 <= '0'; knop3_2 <= '0'; knop3_out <= '0';
		--knop4_0 <= '0'; knop4_1 <= '0'; knop4_2 <= '0'; knop4_out <= '0';
		knop5_0 <= '0'; knop5_1 <= '0'; knop5_2 <= '0'; knop5_3 <= '0'; knop5_4 <= '0'; knop5_5 <= '0'; knop5_out <= '0';
		knop6_0 <= '0'; knop6_1 <= '0'; knop6_2 <= '0'; knop6_out <= '0';
		
		resetled <= '1';

		present_state <= systemreset1_1;

		dubblereset := "0000000000000000";
	
	elsif rising_edge(CLOCK_10) then
	
		if startdubblereset = '1' then
		dubblereset := dubblereset + 1;
			if dubblereset = "0110000110101000" then 
				dubblereset := "0000000000000000";
				setdubblereset <= '1';
			end if;
		else
		setdubblereset <= '0';
		end if;
		
		resetled <= '0';
		
		present_state <= next_state;
		
		--knop1_
			knop1_0 <= not knop1;
			knop1_1 <= knop1_0;
			knop1_2 <= knop1_1;
			knop1_3 <= knop1_2;
			knop1_4 <= knop1_3;
			knop1_5 <= knop1_4;
			knop1_out <= knop1_0 and knop1_1 and knop1_2 and knop1_3 and knop1_4 and knop1_5;
			
		--knop2_
			knop2_0 <= not knop2;
			knop2_1 <= knop2_0;
			knop2_2 <= knop2_1;
			knop2_out <= knop2_0 and knop2_1 and knop2_2;
		
		--knop3_
			knop3_0 <= not knop3;
			knop3_1 <= knop3_0;
			knop3_2 <= knop3_1;
			knop3_out <= knop3_0 and knop3_1 and knop3_2;
		
		--knop4_
		--	knop4_0 <= not knop4;
		--	knop4_1 <= knop4_0;
		--	knop4_2 <= knop4_1;
		--	knop4_out <= knop4_0 and knop4_1 and knop4_2;
			
		--knop5_
			knop5_0 <= not knop5;
			knop5_1 <= knop5_0;
			knop5_2 <= knop5_1;
			knop5_3 <= knop5_2;
			knop5_4 <= knop5_3;
			knop5_5 <= knop5_4;
			knop5_out <= knop5_0 and knop5_1 and knop5_2 and knop5_3 and knop5_4 and knop5_5;
			
		--knop6_
			knop6_0 <= not knop6;
			knop6_1 <= knop6_0;
			knop6_2 <= knop6_1;
			knop6_out <= knop6_0 and knop6_1 and knop6_2;
			

		knop <= knop6_out; -- voor de systemreset
		
	end if;
		
end process;



combout: process (present_state) is

		begin
			case present_state is
			
				when systemreset1_1 => 
					
					systemreset <='0';
				 
				when systemreset0_1 => 
					
					systemreset <='0';
					
				when systemreset1_2 => 
				
					systemreset <='0';
					
				when systemreset0_2 => 
					
					systemreset <='1';
					
				when systemresetdelay => 
					
					systemreset <='1';
				
			end case;
			
	end process;
	
	modebutton  <= knop2_out;
	start_stop  <= knop3_out;
--	Recovery  	<= knop4_out;
	input_up 	<= knop1_out;
	input_down	<= knop5_out;
	
end architecture deb;


