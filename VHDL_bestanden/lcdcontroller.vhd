-- Filename:  lcdcontroller.vhd   
-- Filetype:  vhdl
-- Date:         24/09/2020
-- Description:  VHDL Description for driving an HD44780 based LCD driver
-- Author:       T.westmaas

-- Libraries et al.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity lcdcontroller is
	port (CLOCK		: in std_logic;
			areset	: in std_logic;
			--rpm
			rpmmode	: in std_logic_vector(2 downto 0);
			rpmcount	: in std_logic_vector(19 downto 0);
			--paddle resistance 
			paddleres: in std_logic_vector(3 downto 0);
			calready	: in std_logic;
			--tijd
			maxrpmM	: in std_logic_vector(7 downto 0);
			maxrpmS	: in std_logic_vector(7 downto 0);
			min   	: in std_logic_vector(7 downto 0);
			sec   	: in std_logic_vector(7 downto 0);
			-- LCD 
			lcd_gpio_E   : out std_logic;
			lcd_gpio_RS  : out std_logic;
			lcd_gpio_RW  : out std_logic;
			lcd_gpio_D 	 : inout std_logic_vector(7 downto 0)
			
	);
	
end entity lcdcontroller;

-- The architecture!
architecture hardware of lcdcontroller is
-- Component declaration of the LCD module
component lcd_driver_hd44780_module is
	generic (freq         : integer := 10000;
				areset_pol   : std_logic := '1';
				time_init1   : time := 40 ms;
				time_init2   : time := 4100 ns;
				time_init3   : time := 100 us;
				time_tas     : time := 60 ns;
				time_cycle_e : time := 1000 ns;
				time_pweh    : time := 500 ns;
				time_no_bf   : time := 2 ms;
				cursor_on    : boolean := false;
				blink_on     : boolean := false;
				use_bf       : boolean := true
			  );
	port	  (clk      : in std_logic;
			   areset   : in std_logic;
			   -- User side
			   init     : in std_logic;
  			   data     : in std_logic_vector(7 downto 0);
			   wr       : in std_logic;
			   cls      : in std_logic;
			   home     : in std_logic;
			   goto10   : in std_logic;
			   goto20   : in std_logic;
			   goto30   : in std_logic;
			   busy     : out std_logic;
			   -- LCD side
			   LCD_E    : out std_logic;
			   LCD_RS   : out std_logic;
			   LCD_RW   : out std_logic;
			   LCD_DB   : inout std_logic_vector(7 downto 0)
			  );
end component lcd_driver_hd44780_module;

-- The system's frequency
constant sys_freq : integer := 10000;

signal clk      : std_logic;
signal init     : std_logic;
signal data     : std_logic_vector(7 downto 0);
signal wr       : std_logic;
signal cls      : std_logic;
signal home     : std_logic;
signal goto10   : std_logic;
signal goto20   : std_logic;
signal goto30   : std_logic;
signal busy		 : std_logic;

-- The 5-digit BCD number from switches
signal BCD : unsigned(19 downto 0) := "00000000000000000000" ;

-- The rpmmode from rpmselector
signal mode : unsigned(2 downto 0);

-- The states of the machine
type state_type is (reset,
						  Calibrating0, Calibrating1, Calibrating2, Calibrating3, Calibrating4, Calibrating5, Calibrating6, Calibrating7, Calibrating8, Calibrating9, Calibrating10, Calibrating11, Calibrating12, Calibrating13, Calibrating14,
                    writedataline1_0, writedataline1_1, writedataline1_2, writedataline1_3, writedataline1_4, writedataline1_5, writedataline1_6, writedataline1_7, writedataline1_8, writedataline1_9, writedataline1_10,
						  nexstline2_0, nexstline2_1, nexstline2_2,
						  writedataline2_0, writedataline2_1, writedataline2_2, writedataline2_3, writedataline2_4, writedataline2_5, writedataline2_6, writedataline2_7, writedataline2_8, writedataline2_9, writedataline2_10, writedataline2_11, writedataline2_12, writedataline2_13, writedataline2_14, writedataline2_15,
						  nexstline3_0, nexstline3_1, nexstline3_2,
						  writedataline3_0, writedataline3_1, writedataline3_2, writedataline3_3, writedataline3_4, writedataline3_5, writedataline3_6, writedataline3_7, writedataline3_8, writedataline3_9, writedataline3_10, writedataline3_11, writedataline3_12, writedataline3_13, writedataline3_14, writedataline3_15, writedataline3_16, writedataline3_17, writedataline3_18,
						  nexstline4_0, nexstline4_1, nexstline4_2,
						  writedataline4_0, writedataline4_1, writedataline4_2, writedataline4_3, writedataline4_4, writedataline4_5, writedataline4_6, writedataline4_7, writedataline4_8, writedataline4_9, writedataline4_10, writedataline4_11, writedataline4_12,
						  hold, hold2);
signal state : state_type;

-- Counts delay cycles
signal delay_counter : integer range 0 to 9;

-- This function returns the ASCII 8-bit representation of a character.
function retchar(ch : character := '?') return std_logic_vector is
begin
    return std_logic_vector( to_unsigned( character'pos(ch),8));
end function; 

begin

	-- The clock
	clk <= CLOCK;
	
  -- Set the BCD value from the switches	
	BCD <= unsigned(rpmcount);
	
  -- Set the rpmmode value from rpmselector	
	mode <= unsigned(rpmmode);
	
	-- Use LCD module.
	lcdm : lcd_driver_hd44780_module
	generic map (freq => sys_freq, areset_pol => '1', time_cycle_e => 2000 ns, time_pweh => 500 ns,
					 cursor_on => false, blink_on => false, use_bf => false)
					 
	port map (clk => clk, areset => areset, init => init, data => data, wr => wr, cls => cls,
				 home => home, goto10 => goto10, goto20 => goto20, goto30 => goto30, busy => busy,
				 LCD_E => lcd_gpio_E, LCD_RS => lcd_gpio_RS, LCD_RW => lcd_gpio_RW, LCD_DB => lcd_gpio_D);
				 

	-- The client side
	drive: process (clk, areset) is
	variable dotanimation	:	unsigned(15 downto 0);
	variable calreadyclear	:	std_logic;
	   -- Procedure to write a character to the LCD and tick away some clock cycles
	   --	before waiting  for the busy flag to become true.
		procedure waitforbusyandgotonextstate(next_state : state_type) is
		begin
			if delay_counter = 0 then
				wr <= '1';
				delay_counter <= delay_counter + 1;
			elsif delay_counter = 3 then
				if busy = '0' then
					delay_counter <= 0;
					state <= next_state;
				end if;
			else
				delay_counter <= delay_counter + 1;
			end if;
		end procedure;

	begin
		if areset = '1' then
		   -- outputs
			wr <= '0';
			init <= '0';
			cls <= '0';
			home <= '0';
			goto10 <= '0';
			goto20 <= '0';
			goto30 <= '0';
			data <= retchar('?');
         -- internals
			calreadyclear := '0';
			dotanimation := "0000000000000000";
			delay_counter <= 0;
			state <= reset;
		elsif rising_edge(clk) then
			dotanimation := dotanimation + 1;
			wr <= '0';
			init <= '0';
			cls <= '0';
			home <= '0';
			goto10 <= '0';
			goto20 <= '0';
			goto30 <= '0';
			data <= retchar('?');
			
			case state is
				
				when reset =>
					-- Wait for the LCD module ready
					if busy = '0' and calready = '1' and calreadyclear = '1' then
						state <= writedataline1_0;
					elsif busy = '0' then
						state <= Calibrating0;
					end if;
				
				when Calibrating0 =>
								
								data <= retchar(' ');
								waitforbusyandgotonextstate(Calibrating1);
					
				when Calibrating1 =>
										if calready = '1' then
										calreadyclear := '1';
										data <= retchar(' ');
										else
										data <= retchar('C');
										end if;
										waitforbusyandgotonextstate(Calibrating2);
									
				when Calibrating2 =>
										if calready = '1' then
										data <= retchar(' ');
										else
										data <= retchar('a');
										end if;
										waitforbusyandgotonextstate(Calibrating3);
				
				when Calibrating3 =>
										if calready = '1' then
										data <= retchar(' ');
										else
										data <= retchar('l');
										end if;
										waitforbusyandgotonextstate(Calibrating4);
				
				when Calibrating4 =>
										if calready = '1' then
										data <= retchar(' ');
										else
										data <= retchar('i');
										end if;
										waitforbusyandgotonextstate(Calibrating5);
				
				when Calibrating5 =>
										if calready = '1' then
										data <= retchar(' ');
										else
										data <= retchar('b');
										end if;
										waitforbusyandgotonextstate(Calibrating6);
				
				when Calibrating6 =>
										if calready = '1' then
										data <= retchar(' ');
										else
										data <= retchar('r');
										end if;
										waitforbusyandgotonextstate(Calibrating7);
				
				when Calibrating7 =>
										if calready = '1' then
										data <= retchar(' ');
										else
										data <= retchar('a');
										end if;
										waitforbusyandgotonextstate(Calibrating8);
				
				when Calibrating8 =>
										if calready = '1' then
										data <= retchar(' ');
										else
										data <= retchar('t');
										end if;
										waitforbusyandgotonextstate(Calibrating9);
				
				when Calibrating9 =>
										if calready = '1' then
										data <= retchar(' ');
										else
										data <= retchar('i');
										end if;
										waitforbusyandgotonextstate(Calibrating10);
				
				when Calibrating10 =>
										if calready = '1' then
										data <= retchar(' ');
										else
										data <= retchar('n');
										end if;
										waitforbusyandgotonextstate(Calibrating11);
										
				when Calibrating11 =>
										if calready = '1' then
										data <= retchar(' ');
										else
										data <= retchar('g');
										end if;
										waitforbusyandgotonextstate(Calibrating12);
				
				when Calibrating12 =>
										if calready = '1' then
										data <= retchar(' ');
										elsif dotanimation > "0010011100010000" then
										data <= retchar('.');
										else
										data <= retchar(' ');
										end if;
										waitforbusyandgotonextstate(Calibrating13);
				
				when Calibrating13 =>
										if calready = '1' then
										data <= retchar(' ');
										elsif dotanimation > "0100111000100000"  then
										data <= retchar('.');
										else
										data <= retchar(' ');
										end if;
										waitforbusyandgotonextstate(Calibrating14);
				
				when Calibrating14 =>
										if calready = '1' then
										data <= retchar(' ');
										elsif dotanimation > "0111010100110000" then
										data <= retchar('.');
										waitforbusyandgotonextstate(hold);
										else
										data <= retchar(' ');
										end if;
										
										if dotanimation > "1001110001000000" then
										dotanimation := "0000000000000000";
										end if;
										
										waitforbusyandgotonextstate(hold);
										
--WriteDataline1 RPM
				when writedataline1_0 =>
					if mode = "000" then --huidigrpm
					data <= retchar('R');
					waitforbusyandgotonextstate(writedataline1_1);
					elsif mode = "001" then --maxrpm
					data <= retchar('M');
					waitforbusyandgotonextstate(writedataline1_1);
					elsif mode = "010" then --gemrpm
					data <= retchar('A');
					waitforbusyandgotonextstate(writedataline1_1);
					elsif mode = "111" then --trapweerstand
					data <= retchar('R');
					waitforbusyandgotonextstate(writedataline1_1);
					elsif mode = "011" then --avrcount
					data <= retchar('T');
					waitforbusyandgotonextstate(writedataline1_1);
					end if;
				
			   when writedataline1_1 =>
					if mode = "000" then --huidigrpm
					data <= retchar('P');
					waitforbusyandgotonextstate(writedataline1_2);
					elsif mode = "001" then --maxrpm
					data <= retchar('A');
					waitforbusyandgotonextstate(writedataline1_2);
					elsif mode = "010" then --gemrpm
					data <= retchar('V');
					waitforbusyandgotonextstate(writedataline1_2);
					elsif mode = "111" then --trapweerstand
					data <= retchar('E');
					waitforbusyandgotonextstate(writedataline1_2);
					elsif mode = "011" then --omwcount
					data <= retchar('O');
					waitforbusyandgotonextstate(writedataline1_2);
					end if;

			   when writedataline1_2 =>
					if mode = "000" then --huidigrpm
					data <= retchar('M');
					waitforbusyandgotonextstate(writedataline1_3);
					elsif mode = "001" then --maxrpm
					data <= retchar('X');
					waitforbusyandgotonextstate(writedataline1_3);
					elsif mode = "010" then --avrrpm
					data <= retchar('R');
					waitforbusyandgotonextstate(writedataline1_3);
					elsif mode = "111" then --trapweerstand
					data <= retchar('S');
					waitforbusyandgotonextstate(writedataline1_3);
					elsif mode = "011" then --omwcount
					data <= retchar('T');
					waitforbusyandgotonextstate(writedataline1_3);
					end if;
					
			   when writedataline1_3 =>
					if mode = "000" then --huidigrpm
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline1_4);
					elsif mode = "001" then --maxrpm
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline1_4);
					elsif mode = "010" then --avrrpm
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline1_4);
					elsif mode = "111" then --trapweerstand
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline1_4);
					elsif mode = "011" then --omwcount
					data <= retchar('A');
					waitforbusyandgotonextstate(writedataline1_4);
					end if;
					
			   when writedataline1_4 =>
					if mode = "000" then --huidigrpm
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline1_5);
					elsif mode = "001" then --maxrpm
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline1_5);
					elsif mode = "010" then --avrrpm
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline1_5);
					elsif mode = "111" then --trapweerstand
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline1_5);
					elsif mode = "011" then --omwcount
					data <= retchar('L');
					waitforbusyandgotonextstate(writedataline1_5);
					end if;
					
				when writedataline1_5 =>
					data <= retchar(':');
					waitforbusyandgotonextstate(writedataline1_6);
					
				when writedataline1_6 =>
					if mode = "011" then --omwcount
					data <= X"3" & std_logic_vector(BCD(19 downto 16));
					waitforbusyandgotonextstate(writedataline1_7);
					else
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline1_7);
					end if;
					
				when writedataline1_7 =>
					if mode = "011" then --omwcount
					data <= X"3" & std_logic_vector(BCD(15 downto 12));
					waitforbusyandgotonextstate(writedataline1_8);
					else
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline1_8);
					end if;
					
			   when writedataline1_8 =>
					
					if mode = "111" then --trapweerstand
					data <= X"3" & std_logic_vector(BCD(3 downto 0));
					else
					data <= X"3" & std_logic_vector(BCD(11 downto 8));
					end if;
					waitforbusyandgotonextstate(writedataline1_9);
					
			   when writedataline1_9 =>
					
					if mode = "111" then --trapweerstand
					data <= retchar(' ');
					else
					data <= X"3" & std_logic_vector(BCD(7 downto 4));
					end if;
					waitforbusyandgotonextstate(writedataline1_10);
					
			   when writedataline1_10 =>
					
					if mode = "111" then --trapweerstand
					data <= retchar(' ');
					else
					data <= X"3" & std_logic_vector(BCD(3 downto 0));
					end if;
					waitforbusyandgotonextstate(nexstline2_0);

--nexstline2
				when nexstline2_0 =>
               goto10 <= '1';
					state <= nexstline2_1; 
				
				when nexstline2_1 =>
				   state <= nexstline2_2;
					
				when nexstline2_2 =>
					if busy = '0' then
					    state <= writedataline2_0;
					end if;

--WriteDataline2
				when writedataline2_0 =>
					if mode = "001" then --maxrpm
					data <= retchar('A');
					waitforbusyandgotonextstate(writedataline2_1);
					else
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline2_1);
					end if;
					
				when writedataline2_1 =>
				
					if mode = "001" then --maxrpm
					data <= retchar('t');
					waitforbusyandgotonextstate(writedataline2_2);
					else
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline2_2);
					end if;
					
				when writedataline2_2 =>
				
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline2_3);
					
				when writedataline2_3 =>
				
					if mode = "001" then --maxrpm
					data <= retchar('T');
					waitforbusyandgotonextstate(writedataline2_4);
					else
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline2_4);
					end if;
					
				when writedataline2_4 =>
				
					if mode = "001" then --maxrpm
					data <= retchar('i');
					waitforbusyandgotonextstate(writedataline2_5);
					else
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline2_5);
					end if;
					
				when writedataline2_5 =>
				
					if mode = "001" then --maxrpm
					data <= retchar('m');
					waitforbusyandgotonextstate(writedataline2_6);
					else
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline2_6);
					end if;
					
				when writedataline2_6 =>
				
					if mode = "001" then --maxrpm
					data <= retchar('e');
					waitforbusyandgotonextstate(writedataline2_7);
					else
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline2_7);
					end if;
					
				when writedataline2_7 =>
				
					if mode = "001" then --maxrpm
					data <= retchar(':');
					waitforbusyandgotonextstate(writedataline2_8);
					else
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline2_8);
					end if;
					
				when writedataline2_8 =>
				
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline2_9);
					
				when writedataline2_9 =>
				
					if mode = "001" then --maxrpm
				   data <= X"3" & std_logic_vector(maxrpmM(7 downto 4)); 
					waitforbusyandgotonextstate(writedataline2_10);
					else 
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline2_10);
					end if;
					
				when writedataline2_10 =>
				
					if mode = "001" then --maxrpm
				   data <= X"3" & std_logic_vector(maxrpmM(3 downto 0)); 
					waitforbusyandgotonextstate(writedataline2_11);
					else 
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline2_11);
					end if;
					
				when writedataline2_11 =>
				
					if mode = "001" then --maxrpm
				   data <= retchar(':');
					waitforbusyandgotonextstate(writedataline2_12);
					else 
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline2_12);
					end if;
					
				when writedataline2_12 =>
				
					if mode = "001" then --maxrpm
				   data <= X"3" & std_logic_vector(maxrpmS(7 downto 4)); 
					waitforbusyandgotonextstate(writedataline2_13);
					else 
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline2_13);
					end if;
					
				when writedataline2_13 =>
				
					if mode = "001" then --maxrpm
				   data <= X"3" & std_logic_vector(maxrpmS(3 downto 0)); 
					waitforbusyandgotonextstate(writedataline2_14);
					else 
					data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline2_14);
					end if;
					
				when writedataline2_14 =>
					data <= retchar(' ');
					waitforbusyandgotonextstate(nexstline3_0);

--nexstline3
				when nexstline3_0 =>
               goto20 <= '1';
					state <= nexstline3_1; 
				
				when nexstline3_1 =>
				   state <= nexstline3_2;
					
				when nexstline3_2 =>
					if busy = '0' then
					    state <= writedataline3_0;
					end if;

--WriteDataline3 stopwatch
					when writedataline3_0 =>
				   data <= retchar('S');
					waitforbusyandgotonextstate(writedataline3_1);
					
					when writedataline3_1 =>
				   data <= retchar('t');
					waitforbusyandgotonextstate(writedataline3_2);
					
					when writedataline3_2 =>
				   data <= retchar('o');
					waitforbusyandgotonextstate(writedataline3_3);
					
					when writedataline3_3 =>
				   data <= retchar('p');
					waitforbusyandgotonextstate(writedataline3_4);
					
					when writedataline3_4 =>
				   data <= retchar('W');
					waitforbusyandgotonextstate(writedataline3_5);
					
					when writedataline3_5 =>
				   data <= retchar('a');
					waitforbusyandgotonextstate(writedataline3_6);
					
					when writedataline3_6 =>
				   data <= retchar('t');
					waitforbusyandgotonextstate(writedataline3_7);
					
					when writedataline3_7 =>
				   data <= retchar('c');
					waitforbusyandgotonextstate(writedataline3_8);
					
					when writedataline3_8 =>
				   data <= retchar('h');
					waitforbusyandgotonextstate(writedataline3_9);
					
					when writedataline3_9 =>
				   data <= retchar(':');
					waitforbusyandgotonextstate(writedataline3_10);
					
					when writedataline3_10 =>
				   data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline3_14);
					
					when writedataline3_14 =>
				   data <= X"3" & std_logic_vector(min(7 downto 4)); 
					waitforbusyandgotonextstate(writedataline3_15);
					
					when writedataline3_15 =>
				   data <= X"3" & std_logic_vector(min(3 downto 0)); 
					waitforbusyandgotonextstate(writedataline3_16);
					
					when writedataline3_16 =>
				   data <= retchar(':');
					waitforbusyandgotonextstate(writedataline3_17);
					
					when writedataline3_17 =>
				   data <= X"3" & std_logic_vector(sec(7 downto 4)); 
					waitforbusyandgotonextstate(writedataline3_18);
					
					when writedataline3_18 =>
				   data <= X"3" & std_logic_vector(sec(3 downto 0)); 
					waitforbusyandgotonextstate(nexstline4_0);

--nexstline4
				when nexstline4_0 =>
               goto30 <= '1';
					state <= nexstline4_1; 
				
				when nexstline4_1 =>
				   state <= nexstline4_2;
					
				when nexstline4_2 =>
					if busy = '0' then
					    state <= writedataline4_0;
					end if;


--WriteDataline4 resistance
					when writedataline4_0 =>
				   data <= retchar('R');
					waitforbusyandgotonextstate(writedataline4_1);
					
					when writedataline4_1 =>
				   data <= retchar('e');
					waitforbusyandgotonextstate(writedataline4_2);
					
					when writedataline4_2 =>
				   data <= retchar('s');
					waitforbusyandgotonextstate(writedataline4_3);
					
					when writedataline4_3 =>
				   data <= retchar('i');
					waitforbusyandgotonextstate(writedataline4_4);
					
					when writedataline4_4 =>
				   data <= retchar('s');
					waitforbusyandgotonextstate(writedataline4_5);
					
					when writedataline4_5 =>
				   data <= retchar('t');
					waitforbusyandgotonextstate(writedataline4_6);
					
					when writedataline4_6 =>
				   data <= retchar('a');
					waitforbusyandgotonextstate(writedataline4_7);
					
					when writedataline4_7 =>
				   data <= retchar('n');
					waitforbusyandgotonextstate(writedataline4_8);
					
					when writedataline4_8 =>
				   data <= retchar('c');
					waitforbusyandgotonextstate(writedataline4_9);
					
					when writedataline4_9 =>
				   data <= retchar('e');
					waitforbusyandgotonextstate(writedataline4_10);
					
					when writedataline4_10 =>
				   data <= retchar(':');
					waitforbusyandgotonextstate(writedataline4_11);
					
					when writedataline4_11 =>
				   data <= retchar(' ');
					waitforbusyandgotonextstate(writedataline4_12);
					
					when writedataline4_12 =>
				   data <= X"3" & std_logic_vector(paddleres(3 downto 0));
					waitforbusyandgotonextstate(hold);
					

--finish
				when hold =>
				    home <= '1';
					 state <= hold2;
					 
				when hold2 =>
					 state <= reset;
					 
					 
				when others =>
					 state <= hold;

			end case;
		end if;
	end process;


end architecture hardware;