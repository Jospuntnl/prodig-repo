-- Filename:     tb_Displaydriver.vhd 
-- Filetype:     VHDL testbench
-- Date:         24/09/2020
-- Description:  A testbench of Displaydriver
-- Author:       T.westmaas

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity tb_Displaydriver is
end entity tb_Displaydriver;

architecture sim of tb_Displaydriver is
constant Tperiod : time := 100 us;

component Displaydriver is

	port (CLOCK_10 : in std_logic;
			areset	: in std_logic;
			--trapweerstand
			paddleres  : in std_logic_vector(3 downto 0);
			calready	  : in std_logic;
			--RPM 
			modebutton	: in std_logic;
			huidigrpm	: in std_logic_vector(7 downto 0);
			maxrpm   	: in std_logic_vector(7 downto 0);
			gemrpm   	: in std_logic_vector(7 downto 0);
			omwcount 	: in std_logic_vector(15 downto 0);
			adcdata		: in std_logic_vector(7 downto 0);
			--tijd
			maxrpmM	: in std_logic_vector(7 downto 0);
			maxrpmS	: in std_logic_vector(7 downto 0);
			min   	: in std_logic_vector(7 downto 0);
			sec   	: in std_logic_vector(7 downto 0);
			-- LCD outputs
			lcd_gpio_E   : out std_logic;
			lcd_gpio_RS  : out std_logic;
			lcd_gpio_RW  : out std_logic;
			lcd_gpio_D 	 : inout std_logic_vector(7 downto 0)
			
		  );
end component Displaydriver;

signal huidigrpm, maxrpm, gemrpm, adcdata: std_logic_vector(7 downto 0);
signal clk, areset, lcd_gpio_E, lcd_gpio_RS, lcd_gpio_RW, mode, calready : std_logic;
signal min, sec, maxrpmM, maxrpmS : std_logic_vector(7 downto 0);
signal lcd_gpio_D : std_logic_vector(7 downto 0);
signal paddleres : std_logic_vector(3 downto 0);
signal omwcount 	: std_logic_vector(15 downto 0);


begin

	dut : Displaydriver
	port map (CLOCK_10 => clk, areset => areset, modebutton => mode, huidigrpm => huidigrpm, maxrpm => maxrpm, gemrpm => gemrpm, omwcount => omwcount, adcdata => adcdata, paddleres => paddleres, calready => calready, min => min, sec => sec, maxrpmM => maxrpmM, maxrpmS => maxrpmS, lcd_gpio_E => lcd_gpio_E, lcd_gpio_RS => lcd_gpio_RS, lcd_gpio_RW => lcd_gpio_RW, lcd_gpio_D => lcd_gpio_D );
	
	
clockgen: process is
	begin
		clk <= '0';
		wait for Tperiod/2;
		clk <= '1';
		wait for Tperiod/2;
	end process clockgen;

		
datagen: process is
	begin
		
		areset 		<= '0';
		mode 			<= '0';
		huidigrpm	<= "01101101"; --109
		maxrpm		<= "01111100"; --124
		gemrpm		<= "00111000"; --056
		omwcount		<= "0110010100001000"; --25.864
		paddleres	<= "0000";
		
		wait for 1000 ms;
		
		paddleres	<= "0011";
		mode 			<= '1';
		
		wait for 200 ms;
		
		mode 			<= '0';
		
		wait for 1000 ms;
		
		mode 			<= '1';
		
		wait for 200 ms;
		
		paddleres	<= "0100";
		mode 			<= '0';
		
		wait for 1000 ms;
		
		mode 			<= '1';
		
		wait for 200 ms;
		
		mode 			<= '0';
		
		wait for 1000 ms;
		
		mode 			<= '1';
		
		wait for 200 ms;
		
		mode 			<= '0';
		
		wait for 1000 ms;
		
		mode 			<= '1';
		
		wait for 200 ms;
		
		mode 			<= '0';
		
		wait for Tperiod/4;
		
		areset <= '1';
		wait for 2*Tperiod;
		
		mode 			<= '0';
		huidigrpm	<= "00000000"; --109
		maxrpm		<= "00000000"; --124
		gemrpm		<= "00000000"; --056
		omwcount		<= "0000000000000000"; --25.864
		
		
		areset <= '0';
		wait for 2*Tperiod;
		
	end process;
	
end architecture sim;