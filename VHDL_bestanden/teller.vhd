--Datum: 9-10-20
--Auteur: Jos Knol
--Beschrijving:
--		Een teller om het aantal klokpulsen tussen twee ingangspulsen te meten.
-- Versie: 1.1

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


-- Define de entity.
entity teller is
	port(
		sysclear		: in std_logic;
		hallsens		: in std_logic;
		areset		: in std_logic;
		clk			: in std_logic;
		counter		: out unsigned(31 downto 0);
		rotations	: out unsigned(15 downto 0)
		);
end entity teller;

architecture rtl of teller is
signal lastcount : unsigned(31 downto 0);
signal tempcount : unsigned(31 downto 0);
signal rotation	: unsigned(15 downto 0);

signal s1, s2, s3, dout, flag : std_logic;
begin
	process(clk, areset)
	begin
	if areset = '1' then
		lastcount	<= "00000000000000000000000000000000";
		counter 		<= "00000000000000000000000000000000";
		tempcount 	<= "00000000000000000000000000000000";
		rotation 	<= "0000000000000000";
		s1 <= '0';
		s2 <= '0';
		s3 <= '0';
		dout <= '0';
	elsif rising_edge(clk) then
	
	-- Debounce het ingangssignaal d.m.v. een schuifregister
	
		s1 <= hallsens;
		s2 <= s1;
		s3 <= s2;
		dout <= s1 and s2 and s3;
		
		if sysclear = '1' then
			lastcount	<= "00000000000000000000000000000000";
			counter 		<= "00000000000000000000000000000000";
			tempcount 	<= "00000000000000000000000000000000";
			rotation 	<= "0000000000000000";

		
		elsif dout = '1' and flag = '1' then
		
	-- Als er een input is gedetecteerd, reset teller, geef de nieuwe waarde door aan de output. Gebruik flag om ervoor te zorgen dat dit maar 1 keer gebeurt.	
		
			flag <= '0';
			counter <= lastcount;
			tempcount <= "00000000000000000000000000000000";
			rotation <= rotation + 1;
			
		elsif tempcount >	 "00000000000000000111100100011000" then
			counter <=		 "00000000000000000000000000000000";
			tempcount <=	 "00000000000000000000000000000000";
		elsif dout = '0' then
			flag <= '1';
			tempcount <= tempcount + 1;
			lastcount <= tempcount;
		end if;
	end if;
	

	end process;
	
	-- rotatieteller
	rotations <= rotation;
end rtl;

					
-- "Hoe meer if statements, hoe mooier"
-- 								-- Jos Knol (2020)				
			