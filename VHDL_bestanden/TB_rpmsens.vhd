--Datum: 9-10-20
--Auteur: Jos Knol
--Beschrijving:
--		Een testbench voor het structurele RPMsens project
-- Versie: 1.1

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity TB_rpmsens is
end entity;

architecture sim of TB_rpmsens is

component rpmsens is
	port (clk 			: in std_logic;
			areset		: in std_logic;
			hallsensor	: in std_logic;
			rpm			: out unsigned(7 downto 0);
			rotations	: out unsigned(15 downto 0);
			max_rpm		: out unsigned(7 downto 0);
			maxflag		: out std_logic;
			sysclear		: in std_logic
		  );
end component rpmsens;

signal	clk 			: 	std_logic;
signal 	pulse			: 	std_logic;
signal	areset		:  std_logic;
signal	hallsensor	:  std_logic;
signal	rpm			:  unsigned(7 downto 0);
signal	rotations	:  unsigned(15 downto 0);
signal	max_rpm		:  unsigned(7 downto 0);
signal	maxflag		:  std_logic;
signal	sysclear		:  std_logic;

begin

	dut: rpmsens
	port map (clk => clk, areset => areset, sysclear => sysclear, hallsensor => hallsensor, rpm => rpm, rotations => rotations, max_rpm => max_rpm, maxflag => maxflag);
	
	clockgen: process is
	begin
	
		clk <= '0';
		
		wait for 10 us;
		
		loop
			clk <= '1';
			
			wait for 50 us;
			
			clk <= '0';
			
			wait for 50 us;
		end loop;
	end process;
	datagen: process is
		begin
			areset <= '1';
			
			hallsensor <= '1';
			
			wait for 125 us;
			
			areset <= '0';
			
			wait for 300 us;
			
			hallsensor <= '0';
			
			wait for 300 us;
			
			hallsensor <= '1';
			
			wait for 75 us;
			
			hallsensor <= '0';
			
			wait for 300 us;
			
			hallsensor <= '1';
			
			wait for 0.34 sec;
			
			hallsensor <= '0';
								
			wait for 300 us;
			
			hallsensor <= '1';
			
			wait for 5 us;
			
			hallsensor <= '0';
			
			wait for 300 us;
			
			hallsensor <= '1';
			
			wait for 0.68 sec;
			
			hallsensor <= '0';
			
			wait for 330 us;
			
			hallsensor <= '1';
			
			wait for 0.34 sec;
			
			hallsensor <= '0';
	
			wait;
		end process;
end architecture;