-- Filename:     tb_rpmselector.vhd
-- Filetype:     VHDL testbench
-- Date:         24/09/2020
-- Description:  A testbench of a rpmselector
-- Author:       E. Heijerman

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_rpmselector is
end entity tb_rpmselector;

architecture sim of tb_rpmselector is
constant Tperiod : time := 100 us;

component rpmselector is
    port (clk     : in std_logic;
			areset	: in std_logic;
			mode	   : in std_logic;
			huidigrpm: in std_logic_vector(7 downto 0);
			maxrpm   : in std_logic_vector(7 downto 0);
			gemrpm   : in std_logic_vector(7 downto 0);
			omwcount : in std_logic_vector(15 downto 0);
			
			
			rpmmode	: out std_logic_vector(1 downto 0);
			rpmcount : out std_logic_vector(15 downto 0)
         );
end component rpmselector;

signal clk, areset, mode : std_logic;
signal huidigrpm, maxrpm, gemrpm : std_logic_vector(7 downto 0);
signal omwcount, rpmcount : std_logic_vector(15 downto 0);
signal rpmmode : std_logic_vector(1 downto 0);

begin

	dut : rpmselector
	port map (clk => clk, areset => areset, mode => mode, huidigrpm => huidigrpm, maxrpm => maxrpm, gemrpm => gemrpm, omwcount => omwcount, rpmmode => rpmmode, rpmcount => rpmcount);
	
	
clockgen: process is
	begin
		clk <= '0';
		wait for Tperiod/2;
		clk <= '1';
		wait for Tperiod/2;
	end process clockgen;	
		
datagen: process is
	begin
		
		--load values
		mode 			<= '0';
		huidigrpm	<= "01101101"; --109
		maxrpm		<= "01111100"; --124
		gemrpm		<= "00111000"; --056
		omwcount		<= "0110010100001000"; --25.864
		
		wait until clk = '1'; 
		wait until clk = '1';
		wait until clk = '1';
		wait until clk = '1'; 
		wait until clk = '1';
		
		mode <= '1';
		
		wait until clk = '1'; 
		wait until clk = '1';
		wait until clk = '1';
		wait until clk = '1'; 
		wait until clk = '1';
		
		mode <= '0';
		
		wait until clk = '1'; 
		wait until clk = '1';
		wait until clk = '1';
		wait until clk = '1'; 
		wait until clk = '1';
		
		mode <= '1';
		
		wait until clk = '1'; 
		wait until clk = '1';
		wait until clk = '1';
		wait until clk = '1'; 
		wait until clk = '1';
		
		mode <= '0';
		
		wait until clk = '1'; 
		wait until clk = '1';
		wait until clk = '1';
		wait until clk = '1'; 
		wait until clk = '1';
		
		mode <= '1';
		
		wait until clk = '1'; 
		wait until clk = '1';
		wait until clk = '1';
		wait until clk = '1'; 
		wait until clk = '1';
		
		mode <= '0';
		
		
		wait until clk = '1'; 
		wait until clk = '1';
		wait until clk = '1';
		wait until clk = '1'; 
		wait until clk = '1';
		
		mode <= '1';
		
		wait until clk = '1'; 
		wait until clk = '1';
		wait until clk = '1';
		wait until clk = '1'; 
		wait until clk = '1';
		
		mode <= '0';
		
		
		wait until clk = '1'; 
		wait until clk = '1';
		wait until clk = '1';
		wait until clk = '1'; 
		wait until clk = '1';
		
		mode <= '1';
		
		wait until clk = '1'; 
		wait until clk = '1';
		wait until clk = '1';
		wait until clk = '1'; 
		wait until clk = '1';
		
		mode <= '0';
		
		
		wait until clk = '1'; 
		wait until clk = '1';
		wait until clk = '1';
		wait until clk = '1'; 
		wait until clk = '1';
		
		mode <= '1';
		
		wait until clk = '1'; 
		wait until clk = '1';
		wait until clk = '1';
		wait until clk = '1'; 
		wait until clk = '1';
		
		mode <= '0';
		
		
		wait until clk = '1'; 
		wait until clk = '1';
		wait until clk = '1';
		wait until clk = '1'; 
		wait until clk = '1';
		
		mode <= '1';
		
		wait until clk = '1'; 
		wait until clk = '1';
		wait until clk = '1';
		wait until clk = '1'; 
		wait until clk = '1';
		
		mode <= '0';
		
		
		wait until clk = '1'; 
		wait until clk = '1';
		wait until clk = '1';
		wait until clk = '1'; 
		wait until clk = '1';
		wait for Tperiod/4;
		
		areset <= '1';
		wait for 2*Tperiod;
		
		areset <= '0';
		wait for 2*Tperiod;
		
		wait until clk = '1';
		
	end process;
	
end architecture sim;