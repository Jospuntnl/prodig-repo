-- Filename:     tb_bcd_decoder.vhd 
-- Filetype:     VHDL testbench
-- Date:         11/09/2020
-- Description:  A testbench of bcd_decoder
-- Author:       T.westmaas

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity tb_bcd_decoder is
end entity tb_bcd_decoder;

architecture sim of tb_bcd_decoder is
constant Tperiod : time := 100 us;

component bcd_decoder is

	port  (	binary_in 	: in  std_logic_vector (15 downto 0);
				bcd_out 		: out  std_logic_vector (19 downto 0)
          );
end component bcd_decoder;

signal binary_in : std_logic_vector (15 downto 0);
signal bcd_out : std_logic_vector (19 downto 0);
signal clk : std_logic;
begin

	dut : bcd_decoder
	port map (binary_in => binary_in, bcd_out => bcd_out);
	
	
clockgen: process is
	begin
		clk <= '0';
		wait for Tperiod/2;
		clk <= '1';
		wait for Tperiod/2;
	end process clockgen;

		
datagen: process is
	begin
		
		binary_in <= "0000000000000000";
		
			wait for 1000 ms;
		
		binary_in <= "0000000000011110";
		
			wait for 1000 ms;
		
		binary_in <= "0000110001100100";
		
			wait for 1000 ms;
		
		binary_in <= "0000100011000100";
		
			wait for 1000 ms;
		
		
	end process;
	
end architecture sim;