-- Filename:  bcd_decoder.vhd   
-- Filetype:  vhdl
-- Date:         09/09/2020
-- Description:  VHDL Description 
-- Author:       T.westmaas

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity bcd_decoder is
    Port (	binary_in 	: in  std_logic_vector (15 downto 0);
				bcd_out 		: out  std_logic_vector (19 downto 0)
          );
end bcd_decoder;

architecture decoder of bcd_decoder is

begin

	process	( binary_in)	is 

  -- temporary variable
  variable temp : std_logic_vector (15 downto 0);
  
  -- variable to store the output BCD number
  variable bcd : UNSIGNED (19 downto 0) := (others => '0');
  
  begin
    
	 -- zero the bcd variable
    bcd := (others => '0');
    -- read input into temp variable
    temp(15 downto 0) := binary_in;
    
    -- cycle 16 times as we have 16 input bits
    for i in 0 to 15 loop
    
      if bcd(3 downto 0) > 4 then 
        bcd(3 downto 0) := bcd(3 downto 0) + 3;
      end if;
      
      if bcd(7 downto 4) > 4 then 
        bcd(7 downto 4) := bcd(7 downto 4) + 3;
      end if;
    
      if bcd(11 downto 8) > 4 then  
        bcd(11 downto 8) := bcd(11 downto 8) + 3;
      end if;
    
		if bcd(15 downto 12) > 4 then  
        bcd(15 downto 12) := bcd(15 downto 12) + 3;
      end if;
    
		if bcd(19 downto 16) > 4 then  
        bcd(19 downto 16) := bcd(19 downto 16) + 3;
      end if; 
		
      -- shift bcd left by 1 bit, copy MSB of temp into LSB of bcd
      bcd := bcd(18 downto 0) & temp(15);
    
      -- shift temp left by 1 bit
      temp := temp(14 downto 0) & '0';
    
    end loop;
 
    -- set outputs
    bcd_out <= std_logic_vector(bcd(19 downto 0));

  
  end process;            
  
end architecture;
