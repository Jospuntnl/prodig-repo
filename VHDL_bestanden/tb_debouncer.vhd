-- Filename: 	tb_debouncer.vhd   
-- Filetype: 	vhdl
-- Date:         10/10/2020
-- Description:  VHDL Description 
-- Author:       T.westmaas

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_debouncer is
end entity tb_debouncer;

architecture sim of tb_debouncer is
constant Tperiod : time := 100 us;

component debouncer is
    port (CLOCK_10 : in std_logic;
			--knoppen
			knop1			: in std_logic;--mode
			knop2			: in std_logic;--start/stop
			knop3			: in std_logic;--recovery
			knop4			: in std_logic;--up
			knop5			: in std_logic;--reset
			knop6			: in std_logic;--down
			areset		: in std_logic;--
			
			reset				: out std_logic;
			modebutton  	: out std_logic;
--			start_stop  	: out std_logic;
--			Recovery  		: out std_logic;
			input_up 	 	: out std_logic;
			input_down		: out std_logic
			
		  );
end component debouncer;

signal knop1, knop2, knop3, knop4, knop5, knop6 : std_logic;
signal clk, areset : std_logic;
signal reset, modebutton,  input_up, input_down : std_logic;
--signal start_stop, Recovery : std_logic;


begin

	dut : debouncer
	port map (CLOCK_10 => clk, areset => areset, knop1 => knop1, knop2 => knop2, knop3 => knop3, knop4 => knop4, knop5 => knop5, knop6 => knop6, reset => reset, modebutton => modebutton, input_up => input_up, input_down => input_down);
--				, start_stop => start_stop, Recovery => Recovery);
	
clockgen: process is
	begin
		clk <= '0';
		wait for Tperiod/2;
		clk <= '1';
		wait for Tperiod/2;
	end process clockgen;	
		
datagen: process is
	begin
		
		knop1		<=	'1';
		knop2		<=	'1';	
		knop3		<=	'1';
		knop4		<=	'1';
		knop5		<=	'1';
		knop6		<=	'1';
		areset	<=	'0';
		
		wait for 1000 ms;
		
		knop5		<=	'0';
		knop1		<=	'0';
		
		wait for 1000 ms;	
		
		knop5		<=	'1';
		knop1		<=	'1';
		
		wait for 1000 ms;	
		
		knop5		<=	'0';
		knop1		<=	'0';
		
		wait for 1000 ms;	
		
		knop5		<=	'1';
		knop1		<=	'1';
		
		wait for 1000 ms;	
		
		
		wait for Tperiod/4;
		
		areset <= '1';
		wait for 2*Tperiod;
		
		areset <= '0';
		wait for 2*Tperiod;
		
		wait until clk = '1';
		
	end process;
	
end architecture sim;