-- Author: Ruben La Lau
-- Student electrical engineering at The Hague University
-- Date: 12-10-2020

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Entity van de trapweerstandselector
entity trapweerstandselector is
	port(	
			adc_up		: in std_logic;
			adc_down		: in std_logic;
			ready			: in std_logic;
			sysclear		: in std_logic;
			clk			: in std_logic;
			areset		: in std_logic;
			paddleres	: out unsigned(3 downto 0);
			input_up		: in std_logic;
			input_down	: in std_logic;
			in_adc		: in unsigned (3 downto 0);
			out_up		: out std_logic;
			out_down		: out std_logic
			);
end entity trapweerstandselector; 

-- Beschrijving van de states
architecture weerstandsselectie of trapweerstandselector is
type state_type is (startup, positie_0, positie_0_up, positie_1_down, positie_1, positie_1_up, positie_2_down, positie_2, positie_2_up, positie_3_down, positie_3, positie_3_up, positie_4_down, positie_4, positie_4_up, positie_5_down, positie_5, positie_5_up, positie_6_down, positie_6, positie_6_up, positie_7_down, positie_7, positie_7_up);
signal current_state : state_type; 

begin
	process (areset, clk) is
		
		begin
		
		if areset = '1' then
			current_state <= startup;
			out_up <= '0';
			out_down <= '0';
			paddleres <= "0011";
			
		elsif rising_edge(clk) then
			
				case current_state is
					when startup =>
										if adc_up = '1' then	
											out_up <= '1';
											out_down <= '0';
										elsif adc_down = '1' then	
											out_up <= '0';
											out_down <= '1';
										elsif ready = '1' then	
											current_state <= positie_3;
										end if;
						
						
					when positie_0 =>
										if input_up = '1' then		--0
											current_state <= positie_0_up;
										else
											current_state <= positie_0;
										end if;
										
										if in_adc > "0000" then
											out_up <= '0';
											out_down <= '1';
										else
											out_up <= '0';
											out_down <= '0';
										end if;
										
										paddleres <= "0000";
										
					when positie_0_up => 
										if input_up = '0' then
											current_state <= positie_1;
										else
											current_state <= positie_0_up;
										end if;
										
					when positie_1_down =>
										if input_down = '0' then
											current_state <= positie_0;
										else
											current_state <= positie_1_down;
										end if;
										
					when positie_1 =>	if input_up = '1' then		--1
											current_state <= positie_1_up;
										elsif input_down = '1' then
											current_state <= positie_1_down;
										else
											current_state <= positie_1;
										end if;
										
										if (in_adc < "0001") then
											out_up <= '1';
											out_down <= '0';
										elsif (in_adc > "0001") then
											out_up <= '0';
											out_down <= '1';
										else
											out_up <= '0';
											out_down <= '0';
										end if;
										paddleres <= "0001";
										
					when positie_1_up =>
										if input_up = '0' then
											current_state <= positie_2;
										else
											current_state <= positie_1_up;
										end if;
										
					when positie_2_down =>
										if input_down = '0' then
											current_state <= positie_1;
										else
											current_state <= positie_2_down;
										end if;
										
					when positie_2 => 	if  input_up = '1' then		--2
											current_state <= positie_2_up;
										elsif input_down = '1' then
											current_state <= positie_2_down;
										else
											current_state <= positie_2;
										end if;
										
										if (in_adc < "0010") then
											out_up <= '1';
											out_down <= '0';
										elsif (in_adc > "0010") then
											out_up <= '0';
											out_down <= '1';
										else
											out_up <= '0';
											out_down <= '0';
										end if;
										paddleres <= "0010";
										
					when positie_2_up => 
										if input_up = '0' then
											current_state <= positie_3;
										else
											current_state <= positie_2_up;
										end if;
					
					when positie_3_down =>
										if input_down = '0' then
											current_state <= positie_2;
										else
											current_state <= positie_3_down;
										end if;
										
					when positie_3 =>
										if  input_up = '1' then		--3
											current_state <= positie_3_up;
										elsif input_down = '1' then
											current_state <= positie_3_down;
										else
											current_state <= positie_3;
										end if;
										
										if (in_adc < "0011") then
											out_up <= '1';
											out_down <= '0';
										elsif (in_adc > "0011") then
											out_up <= '0';
											out_down <= '1';
										else
											out_up <= '0';
											out_down <= '0';
										end if;
										paddleres <= "0011";
										
					when positie_3_up => 
										if input_up = '0' then
											current_state <= positie_4;
										else
											current_state <= positie_3_up;
										end if;
										
					when positie_4_down => 	
										if input_down = '0' then
											current_state <= positie_3;
										else
											current_state <= positie_4_down;
										end if;
					
					when positie_4 => 
										if  input_up = '1' then		--4
											current_state <= positie_4_up;
										elsif input_down = '1' then
											current_state <= positie_4_down;
										else
											current_state <= positie_4;
										end if;
										
										if (in_adc < "0100") then
											out_up <= '1';
											out_down <= '0';
										elsif (in_adc > "0100") then
											out_up <= '0';
											out_down <= '1';
										else
											out_up <= '0';
											out_down <= '0';
										end if;
										paddleres <= "0100";
										
					when positie_4_up => 	
										if input_up = '0' then
											current_state <= positie_5;
										else 
											current_state <= positie_4_up;
										end if;
										
					when positie_5_down =>
										if input_down = '0' then
											current_state <= positie_4;
										else
											current_state <= positie_5_down;
										end if;
										
					when positie_5 => 	if  input_up = '1' then		--5
											current_state <= positie_5_up;
										elsif input_down = '1' then
											current_state <= positie_5_down;
										else
											current_state <= positie_5;
										end if;
										
										if (in_adc < "0101") then
											out_up <= '1';
											out_down <= '0';
										elsif (in_adc > "0101") then
											out_up <= '0';
											out_down <= '1';
										else
											out_up <= '0';
											out_down <= '0';
										end if;
										paddleres <= "0101";
										
					when positie_5_up => 
										if input_up = '0' then
											current_state <= positie_6;
										else
											current_state <= positie_5_up;
										end if;
										
					when positie_6_down =>
										if input_down = '0' then
											current_state <= positie_5;
										else
											current_state <= positie_6_down;
										end if;
									
					when positie_6 => 	if  input_up = '1' then		--6
											current_state <= positie_6_up;
										elsif input_down = '1' then
											current_state <= positie_6_down;
										else
											current_state <= positie_6;
										end if;
										
										if (in_adc < "0110") then
											out_up <= '1';
											out_down <= '0';
										elsif (in_adc > "0110") then
											out_up <= '0';
											out_down <= '1';
										else
											out_up <= '0';
											out_down <= '0';
										end if;
										paddleres <= "0110";
										
					when positie_6_up => 
										if input_up = '0' then
											current_state <= positie_7;
										else
											current_state <= positie_6_up;
										end if;
										
					when positie_7_down => 
										if input_down = '0' then
											current_state <= positie_6;
										else
											current_state <= positie_7_down;
										end if;
					
					when positie_7 => if  input_up = '1' then		--7
											current_state <= positie_7_up;
										elsif  input_down = '1' then
											current_state <= positie_7_down;
										else
											current_state <= positie_7;
										end if;
										
										if (in_adc < "0111") then
											out_up <= '1';
											out_down <= '0';
										elsif (in_adc > "0111") then
											out_up <= '0';
											out_down <= '1';
										else
											out_up <= '0';
											out_down <= '0';
										end if;
										paddleres <= "0111";
										
					when positie_7_up => 
										if input_up = '0' then
											current_state <= positie_0;
										else
											current_state <= positie_7_up;
										end if;
										
					when others => 
										out_up <= '0';
										out_down <= '0';					
										
				end case;
		end if;
	end process;
end architecture;