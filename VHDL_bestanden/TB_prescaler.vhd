--Datum: 9-10-20
--Auteur: Jos Knol
--Beschrijving:
--		Een testbench voor de prescaler
-- Versie: 1.1

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity TB_prescaler is
end entity;

architecture sim of TB_prescaler is

component prescaler is
	port (clkin  : in std_logic;
			areset : in std_logic;
			clkout : out std_logic
		  );
end component prescaler;

signal clkin	: std_logic;
signal clkout	: std_logic;
signal areset	: std_logic;

begin

	dut: prescaler
	port map (clkin => clkin, clkout => clkout, areset => areset);
	
	clockgen: process is
	begin
	
		clkin <= '0';
		
		wait for 10 ns;
		
		loop
			clkin <= '1';
			
			wait for 10 ns;
			
			clkin <= '0';
			
			wait for 10 ns;
		end loop;
	end process;
	datagen: process is
		begin
			areset <= '1';

			
			wait for 10 ns;
			
			areset <= '0';
			
			wait;
		end process;
end architecture;