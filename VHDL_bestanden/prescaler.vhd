--Datum: 9-10-20
--Auteur: Jos Knol
--Beschrijving:
--Een prescaler om de klokfrequentie te verlagen van 50 MHz naar 10 kHz.
--Versie: 1.1
--Gebaseerd op: DIGSE1; viercijferteller met prescaler en display, door Jesse op den Brouw

library ieee;
use ieee.std_logic_1164.all;


-- Define de entity
entity prescaler is
	port (clkin  : in std_logic;
			clkout : out std_logic;
			areset : in std_logic
		  );
end entity prescaler;


architecture behav of prescaler is
-- Declare het telsignaal
signal count : integer range 0 to 2500;
begin
	process (clkin, areset) is
	variable clkint	: std_logic;
-- Tel tot 2499, daarna reset en begin opnieuw.
	begin
		if areset = '1' then
			count <= 0;
			clkint := '0'; 	 	 
		elsif rising_edge(clkin) then
			if (count = 2499) then
				clkint := not clkint;
				count <= 0;
			else
				count <= count + 1;
			end if;
		end if;
		
		clkout <= clkint;
	end process;

end behav;