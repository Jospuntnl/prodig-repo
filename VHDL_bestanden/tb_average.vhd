library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_average is
end entity tb_average;

architecture sim of tb_average is
		signal	areset	:	std_logic;
		signal	sysclear :	std_logic;
		signal	CLOCK_10	:	std_logic;
		signal	secpuls  : 	std_logic;
		signal	rpm		:	unsigned(7 downto 0);
		signal	aver	:	unsigned(7 downto 0);
	
component average is
	port
	(
		areset		:	in std_logic;
		sysclear 	: 	in std_logic;
		CLOCK_10		:	in std_logic;
		secpuls   	:	in std_logic;
		rpm			:	in unsigned(7 downto 0);
		aver			:	out unsigned(7 downto 0)
	);
end component average;

begin
	dut: average
	port map (CLOCK_10 => CLOCK_10, areset => areset, sysclear => sysclear, secpuls => secpuls, rpm => rpm, aver => aver);
	
	clockgen: process is
	begin
		CLOCK_10 <= '0';
		wait for 50 us;
		CLOCK_10 <= '1';
		wait for 50 us;
	end process clockgen;
	
	secgen: process is
	begin
		secpuls <= '0';
		wait for 250 ms;
		secpuls <= '1';
		wait until CLOCK_10 = '1';
	end process secgen;
	
	datagen: process is
	begin
		rpm	<= "00000000";
		areset <= '1';
		sysclear <= '1';
		wait until CLOCK_10 = '1';
		areset <= '0';
		sysclear <= '0';
		wait until CLOCK_10 = '1';
		rpm	<= "11001000";
		wait until secpuls = '0';
		rpm	<= "00000010";
		wait until secpuls = '0';
		rpm	<= "01100100";
		wait until secpuls = '0';
		rpm	<= "00111100";
		wait until secpuls = '0';
		rpm	<= "00100100";
		wait until secpuls = '0';
		rpm	<= "10100100";
		wait;
	end process datagen;
end sim;
	