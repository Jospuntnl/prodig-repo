--Datum: 9-10-20
--Auteur: Jos Knol
--Beschrijving:
--		Een testbench voor de teller
-- Versie: 1.1

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity TB_teller is
end entity;

architecture sim of TB_teller is

component teller is
	Port (			
		sysclear		: in std_logic;
		hallsens		: in std_logic;
		areset		: in std_logic;
		clk			: in std_logic;
		counter		: out unsigned(31 downto 0);
		rotations	: out unsigned(15 downto 0)
				);
end component teller;

signal clk 			: std_logic;
signal areset 		: std_logic;
signal hallsens 	: std_logic;
signal sysclear	: std_logic;
signal counter 	: unsigned(31 downto 0);
signal rotations	: unsigned(15 downto 0);
signal s1, s2, s3, dout, flag : std_logic;
signal lastcount : unsigned(31 downto 0);
signal tempcount : unsigned(31 downto 0);

begin

	dut: teller
	port map (clk => clk, areset => areset, sysclear => sysclear, hallsens => hallsens, counter => counter, rotations => rotations);
	
	clockgen: process is	
	begin
	
		clk <= '0';
		
		wait for 10 us;
		
		loop
			clk <= '1';
			
			wait for 50 us;
			
			clk <= '0';
			
			wait for 50 us;
		end loop;
	end process;
	datagen: process is
		begin
			areset <= '1';
			sysclear	<= '1';
			
			hallsens <= '0';
			
			wait for 30 ns;
			
			areset <= '0';
			sysclear	<= '0';
			
			wait for 300 us;
			
			hallsens <= '1';
			
			wait for 300 us;
			
			hallsens <= '0';
			
			wait for 75 us;
			
			hallsens <= '1';
			
			wait for 300 us;
			
			hallsens <= '0';
			
			wait for 0.34 sec;
			
			hallsens <= '1';
								
			wait for 300 us;
			
			hallsens <= '0';
			
			wait for 5 us;
			
			hallsens <= '1';
			
			wait for 300 us;
			
			hallsens <= '0';
			
			wait for 0.33 sec;
			
			hallsens <= '1';
			
			wait for 330 us;
			
			hallsens <= '0';
			
			wait for 0.34 sec;
			
			hallsens <= '1';
			wait;
		end process;
end architecture;