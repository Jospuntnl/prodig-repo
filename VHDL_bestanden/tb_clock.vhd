library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_clock is
end entity tb_clock;

architecture sim of tb_clock is
	signal 	pauze		: 	std_logic;
	signal	stop		: 	std_logic;
	signal	clk		:	std_logic;
	signal	reset		:	std_logic;
	signal	seconds	:	unsigned(7 downto 0);
	signal	minutes	:	unsigned(7 downto 0);
	signal 	secpuls	:	std_logic;
	signal 	sysclear	: 	std_logic;
	signal	start_stop: std_logic;
	signal 	maxflag	: std_logic;
	signal 	maxrpmM	: unsigned(7 downto 0);
	signal 	maxrpmS	: unsigned(7 downto 0);

component clock is
	port
	(
		clk 			: in	std_logic;
		sysclear		: in	std_logic;
		reset			: in	std_logic;
		start_stop	: in	std_logic;
		maxflag		: in 	std_logic;
		seconds		: out unsigned(7 downto 0);
		minutes		: out unsigned(7 downto 0);
		secpuls		: out std_logic;
		maxrpmM		: out unsigned(7 downto 0);
		maxrpmS		: out unsigned(7 downto 0)
	);
end component clock;

begin
	dut: clock
	port map (start_stop => start_stop, clk => clk, reset => reset, seconds => seconds, minutes => minutes, secpuls => secpuls, sysclear => sysclear, maxflag => maxflag, maxrpmM => maxrpmM, maxrpmS => maxrpmS);
	
	clockgen: process is
	begin
		clk <= '0';
		wait for 50 us;
		clk <= '1';
		wait for 50 us;
	end process clockgen;
	
	datagen: process is
	begin
		reset <= '1';
		sysclear <= '1';
		wait until clk = '1';
		reset <= '0';
		sysclear <= '0';
		wait for 50 ms;
		start_stop <= '1';
		wait for 50 ms;
		start_stop <= '0';
		wait for 65000 ms;
		maxflag <= '1';
		wait for 50 ms;
		maxflag <= '0';
		wait;
	end process datagen;
end sim;
	