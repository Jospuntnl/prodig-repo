-- Author: Ruben La Lau
-- Student electrical engineering at The Hague University
-- Date: 12-10-2020

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity trapweerstand is

-- Entity beschrijving van de poorten

	port (
			sysclear			: in std_logic;
			clk				: in std_logic;
			input_up			: in std_logic;
			input_down		: in std_logic;
			in_adc			: in unsigned(7 downto 0);
			calready			: out std_logic;
			paddleres		: out unsigned(3 downto 0);
			out_measured	: out std_logic_vector(7 downto 0);
			motor_up			: out std_logic;
			motor_down		: out std_logic;
			busy				: in std_logic;
			readdata			: out std_logic;
			areset			: in std_logic;
			convstart		: out std_logic
		  );
end entity trapweerstand;

architecture structural of trapweerstand is

-- Architecture van de Structural waarin de poorten van de componenten worden beschreven.

component trapweerstandselector is
		port(	
			adc_up		: in std_logic;
			adc_down		: in std_logic;
			ready			: in std_logic;
			sysclear		: in std_logic;
			clk			: in std_logic;
			areset		: in std_logic;
			paddleres	: out unsigned(3 downto 0);
			input_up		: in std_logic;
			input_down	: in std_logic;
			in_adc		: in unsigned (3 downto 0);
			out_up		: out std_logic;
			out_down		: out std_logic
			);
end component trapweerstandselector;

component adc is	
	port( clk				: in std_logic;
			areset			: in std_logic;
			busy				: in std_logic;
			data				: in unsigned(7 downto 0);
			readdata			: out std_logic;
			dataout			: out unsigned(7 downto 0);
			convstart		: out std_logic;
			ready				: out std_logic;
			out_up			: out std_logic;
			out_down			: out std_logic
	);
end component adc;

signal out_adc		: unsigned(7 downto 0);
signal ready_2_ready, out_up_2_adc_up,	out_down_2_adc_down	: std_logic;

-- De port maps van de componenten
begin
	u0: trapweerstandselector
		port map (areset => areset, sysclear => sysclear, clk => clk, input_up => input_up, input_down => input_down, in_adc => out_adc(3 downto 0), paddleres => paddleres, out_up => motor_up, out_down => motor_down, adc_up => out_up_2_adc_up, adc_down => out_down_2_adc_down, ready => ready_2_ready);
	
	u1: adc
		port map	(areset => areset, clk => clk, dataout => out_adc, data => in_adc, readdata => readdata, busy => busy, convstart => convstart, out_up => out_up_2_adc_up, out_down => out_down_2_adc_down, ready => ready_2_ready);
		
	out_measured <= std_logic_vector(out_adc);
	calready <= ready_2_ready;
	
end structural;