-- File: tb_trapweerstand.vhd
-- Author: Ruben La Lau
-- Student electrical engineering at The Hague University
-- Date: 12-10-2020

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_trapweerstand is
end entity tb_trapweerstand;

architecture sim of tb_trapweerstand is

	
component trapweerstand is
	port (
			sysclear			: in std_logic;
			clk				: in std_logic;
			input_up			: in std_logic;
			input_down		: in std_logic;
			in_adc			: in unsigned(7 downto 0);
			calready			: out std_logic;
			paddleres		: out unsigned(3 downto 0);
			out_measured	: out std_logic_vector(7 downto 0);
			motor_up			: out std_logic;
			motor_down		: out std_logic;
			busy				: in std_logic;
			readdata			: out std_logic;
			areset			: in std_logic;
			convstart		: out std_logic
		  );
end component trapweerstand;

signal			sysclear			: std_logic;
signal			clk				: std_logic;
signal			input_up			: std_logic;
signal			input_down		: std_logic;
signal			in_adc			: unsigned(7 downto 0);
signal			calready			: std_logic;
signal			paddleres		: unsigned(3 downto 0);
signal			out_measured	: std_logic_vector(7 downto 0);
signal			motor_up			: std_logic;
signal			motor_down		: std_logic;
signal			busy				: std_logic;
signal			readdata			: std_logic;
signal			areset			: std_logic;
signal			convstart		: std_logic;

begin
	dut : trapweerstand
	port map (sysclear => sysclear, clk => clk, input_up => input_up, input_down => input_down, in_adc => in_adc, calready => calready, paddleres => paddleres, out_measured => out_measured, 
	motor_up => motor_up, motor_down => motor_down, busy => busy, readdata => readdata, areset => areset, convstart => convstart);
	
	clockgen : process is
	begin
		clk <= '0';
		wait for 50 us;
		clk <= '1';
		wait for 50 us;
	end process clockgen;
	
	datagen : process is
	begin
		input_up <= '0';
		input_down <= '0';
		in_adc <= "11111111";
		areset <= '1';
		wait until clk = '1';
		
		wait for 5 ns;
		
		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';
		
		wait for 5 ns;
		
		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;

		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;

		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '1';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;

		input_up <= '0';
		input_down <= '1';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '0';
		input_down <= '1';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '0';
		input_down <= '1';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '0';
		input_down <= '1';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '0';
		input_down <= '1';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '0';
		input_down <= '1';
		in_adc <= "00000000";
		areset <= '0';
		wait until clk = '1';

		wait for 5 ns;
		
		input_up <= '0';
		input_down <= '0';
		in_adc <= "00000000";
		areset <= '1';
		wait until clk = '1';

		wait for 5 ns;
		
		wait;
		
	end process datagen;
	
end sim;